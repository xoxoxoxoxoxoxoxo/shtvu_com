
<style>
.navbar .nav > li.user .dropdown-menu {
	margin: 0;
}
.navbar .nav > li.user:hover .dropdown-menu {
	display: block;
}
.header .brand {
    margin-top: -7px;
    padding-left: 15px;
    color: #fff;
}
</style>
<div class="header navbar navbar-inverse navbar-fixed-top">
        <!-- BEGIN TOP NAVIGATION BAR -->
        <div class="navbar-inner">
                <div class="container-fluid">
                        <!-- BEGIN LOGO -->
                        <a class="brand" href="index">
                        	网站后台管理
                        </a>
                        <!-- END LOGO -->
                        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                        <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                        <img src="/admin/image/menu-toggler.png" alt="" />
                        </a>
                        <!-- END RESPONSIVE MENU TOGGLER -->
                        <!-- BEGIN TOP NAVIGATION MENU -->
                        <ul class="nav pull-right">                        		
                                <!-- BEGIN USER LOGIN DROPDOWN -->
                                <#if isLogin?default(false)>
                                        <li class="dropdown user">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <span class="username"><i class="icon-user" style="font-size:23px;"></i>当前用户：${user?default('')}</span>
                                                <i class="icon-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu">  
						                           <li>
						                                <a href="/home/index"><i class="icon-windows"></i> 管理中心</a>
						                            </li>
						                            <li>
						                                <a href="/admin/logout"><i class="icon-signout"></i> 安全退出</a>
						                            </li>
						                        </ul>
                                        </li>
                                </#if>
                                <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                        <!-- END TOP NAVIGATION MENU -->
                </div>
        </div>
        <!-- END TOP NAVIGATION BAR -->
</div>