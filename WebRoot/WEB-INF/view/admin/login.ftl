<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>管理后台登录 - 我的网站</title>
<link rel="stylesheet" type="text/css" href="/media/css/admin_login.min.css" />
	<script type="text/javascript" src="/media/js/jquery.js"></script>
</head>
<body>
<div class="wrapper">
	<form id="formLogin" action="/admin/login" method="post">
	<div style="float:right">
		<div class="label"><label>帐号：</label></div>
		<div class="enter"><input id="user_name" name="user_name"class="text"  type="text" /></div>
		<div class="label"><label>密码：</label></div>
		<div class="enter"><input id="password" name="password" class="text" type="password" /> /></div>
		<input id="submit" class="button" type="submit" value="登录" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</div>
	<div class="info">
		<div class="copyright"><span style="color:red;margin-left:0px;font-size:12px;">${error?default('')}</span></div>
	</div>
	</form>
</div>
</body>
</html>