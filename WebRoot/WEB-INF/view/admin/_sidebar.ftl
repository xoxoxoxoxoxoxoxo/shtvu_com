<div class="page-sidebar nav-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <ul id="page_sidebar" class="page-sidebar-menu">
        <li>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler hidden-phone">
            </div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li  class="start active">
            <a href="javascript:;">
            <i class="icon-home"></i> 
            <span class="title">公司信息</span>            		
            <span class="arrow "></span>		            
            <span class="selected"></span>
            </a>
            <ul class="sub-menu">            
                <li>
                    <a href="admin/cont" target="mainFrame">公司介绍</a>
                </li>  
                <li>
                    <a href="admin/intro" target="mainFrame">联系我们</a>
                </li>   
            </ul>
        </li>
        <li>
            <a href="javascript:;">
            <i class="icon-cogs"></i> 
            <span class="title">新闻系统</span>            		
            <span class="arrow "></span>		            
            <span class="selected"></span>
            </a>
            <ul class="sub-menu">         
                <li>
                    <a href="admin/news/newsType" target="mainFrame">分类管理</a>
                </li>
                <li>
                    <a href="admin/news/newsInfo" target="mainFrame">添加文章</a>
                </li>
                <li>
                    <a href="admin/news/newsList" target="mainFrame">新闻列表</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;">
            <i class="icon-cogs"></i> 
            <span class="title">产品系统</span>            		
            <span class="arrow "></span>		            
            <span class="selected"></span>
            </a>
            <ul class="sub-menu">         
                <li>
                    <a href="admin/product/typeList" target="mainFrame">产品类别</a>
                </li>
                <li>
                    <a href="admin/product/proInfo" target="mainFrame">添加产品</a>
                </li>
                <li>
                    <a href="admin/product/proList" target="mainFrame">产品列表</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;">
            <i class="icon-cogs"></i> 
            <span class="title">图片系统</span>            		
            <span class="arrow "></span>		            
            <span class="selected"></span>
            </a>
            <ul class="sub-menu">         
                <li>
                    <a href="admin/slide/slideList" target="mainFrame">幻灯片列表</a>
                </li>
                <li>
                    <a href="admin/slide/slideInfo" target="mainFrame">添加幻灯片</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>