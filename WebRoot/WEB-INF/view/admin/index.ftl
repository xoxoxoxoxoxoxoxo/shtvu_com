<#include "../common/_home.ftl">
<@html>
<@head>	
	<link rel="stylesheet" type="text/css" href="admin/css/default.css">
</@head>

<body class="page-header-fixed page-sidebar-fixed">
	<#assign active_menu="home">
	<#include "_header.ftl" />	
	
	<#include "_sidebar.ftl" />		
	
	<!-- BEGIN CONTAINER -->

	<div class="page-container">	
		<div class="page-content" id="page_content">	
			<iframe id="mainFrame" name="mainFrame" scrolling="auto" marginwidth="0" marginheight="0" frameborder="no" src="admin/intro" style="height: 100%; width: 100%;z-index: 5;overflow-x:hidden"></iframe>
		</div>		
	</div>
	<!-- END CONTAINER -->	


	<@plugin/>	
	<script type="text/javascript" src="/admin/js/app.js" ></script>	
	

	<script>		
	
	jQuery(document).ready(function() {    
		App.init(); 	  		
		$("#page_sidebar").find("a[target='mainFrame']").each(function(){
			var href= $(this).attr("href");
			if (href.length == 0 || href.indexOf("javascript") > -1) return;				
            rand = Math.random();
            if (href.indexOf("?") > -1) {
                $(this).attr("href", href + "&" + rand);
            }else {
                $(this).attr("href", href + "?" + rand);
            }
		});
	});

</script>
</body>
</@html>