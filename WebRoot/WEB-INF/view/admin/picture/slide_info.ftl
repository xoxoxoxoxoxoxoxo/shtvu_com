<#include "../../common/_home.ftl">
<@html>
<@head>
	<link rel="stylesheet" type="text/css" href="/media/css/msg_preview.css">
	<script type="text/javascript" src="/js/angular/angular.min.js"></script>
	<script type="text/javascript" src="/js/angular/angular.extend.js"></script>
</@head>
    <style>
    body{color:#333;}
	div, input, select, textarea, span, img, table, td, th, p, a, button, ul, li {
    border-radius: 0px !important;
    font-family:'Microsoft YaHei';
	}
	.returnl{position:absolute;right:20px;top:14px;}
	.breadcrumb{
		background:none;
		padding:0px;
		margin:0px;
		position:absolute;
		top:24px;
		left:130px;
	}
    </style>
<body ng-app="app">
<div class="container-fluid" ng-controller="SlideController">
	<div class="row-fluid">
			<div class="span12">
				<h3 class="page-title">图片系统</h3>
				<hr>
				<div class="returnl">
                    <a class="btn blue" href="admin/slide/slideList/${pageNo?default(1)}"><i class="icon-reorder"></i>返回列表</a>
                </div>
			</div>
	</div>
	<ul class="breadcrumb">
					<li>
						<a href="admin/slide/slideList">幻灯片列表</a>
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="javascript:void()">编辑</a>
					</li>
		     </ul>
	<div class="clearfix"></div>
	<!-- BEGIN PAGE BODY-->
 	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN SAMPLE FORM PORTLET-->   

				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					<form id="form" name="slideForm" class="form-horizontal"  ng-submit="save();">
						
						<div class="control-group">
							<label class="control-label">幻灯片名称<span class="required">*</span> ：</label>
								<div class="controls">
								<input type="text" name="slideTitle" ng-model="slide.name" placeholder="名称不超过30个字符" required ng-maxlength="30">
								<label style="color:red;" ng-if="slideForm.slideTitle.$error.maxlength">品牌名称长度超过30位</label>
							</div>
						</div>
						<div class="control-group">
								<label class="control-label" for="title">图片<span class="required">*</span> ：</label>
								<div class="controls">
										<div class="frm_input_box" style="width:200px;">
												<div class="upload_area">
													<a ng-file-select="onFileSelect($files)" class="upload_access" accept="image/*">上传</a>
													<label>建议尺寸：宽940 * 高400</label>
												</div>
												<p class="js_cover upload_preview">
													<img id="logo_id" ng-src="{{slide.image | imgFilter}}" class="img-polaroid" >	
												</p>
										</div>
								</div>
						  </div>
						  <div class="control-group">
							<label class="control-label">幻灯片说明<span class="required">*</span> ：</label>
							<div class="controls">
							  <textarea name="slideRemark" placeholder="幻灯片说明" type="text" ng-model="slide.remark" ng-maxlength="100" required></textarea>
							  <label style="color:red;" ng-if="slideForm.slideRemark.$error.maxlength">文章摘要超过100字符</label>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">状态<span class="required">*</span> ：</label>
							<div class="controls">
								<select ng-model="slide.status">
									<option value="0">隐藏</option>
									<option value="1">显示</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">显示序列：</label>
							<div class="controls">
							  <input type="number" name="slideSort" ng-model="slide.sort" placeholder="数值越小越靠前" required ng-minlength="1">
							  <label style="color:red;" ng-if="slideForm.slideSort.$error.minlength">不能低于两位数</label>
							</div>
						</div>
						      <button type="submit" class="btn blue"  style="margin-right:10px;margin-left:160px;">保存</button>
						       <a href="admin/slide/slideList" class="btn">返回</a>
						
					  </form>
					<!-- END FORM-->  

			</div>

			<!-- END SAMPLE FORM PORTLET-->

		</div>

	</div>
 	<!-- END PAGE BODY-->
	<div class="clearfix"></div>
</div>
	<@plugin/>
	<script type="text/javascript" src="/media/js/app.js" ></script>
	<script type="text/javascript" src="/js/angular/angular-file-upload.min.js"></script>

	<!-- END CORE PLUGINS -->
<script text/javascript>
	var app = angular.module('app',['ngFileUpload']);
	app.controller('SlideController',function($scope, $http, $upload){
		<#if slideInfo?exists>
		$scope.slide = ${slideInfo};
		<#else>
		$scope.slide = {status:1};
		</#if>
		$scope.save=function(){
			if ($scope.slideForm.$pristine) {
				alert("请修改后再保存！");
				return;
			}
			if ($scope.slideForm.$invalid) {
				alert("请输入正确的设置内容！");
				return;
			}
			jQuery.post("admin/slide/slideEdit",angular.toModel($scope.slide,"slide"),function(data){
				if(data.status == 1){
					alert("保存成功!");
					if(!$scope.slide.id){
						$scope.slide.id = data.id;
					}
				}else{
					alert(data.info);
				}			
			});
		};	
		
		$scope.onFileSelect = function(files){
			var file = files[0];
			if (file == null) return;
			
			
			var pageContent = $(document.body);
			App.blockUI(pageContent, false);
			
			$scope.upload = $upload.upload({
		        url: "/admin/slide/uploadImg?key=slide&path=slide", 
		        file: file, 
		        fileName: file.name 
		    }).progress(function(evt) {        
		    }).success(function(data, status, headers, config) {        
		    	
		    	App.unblockUI(pageContent);
		        
		        if (data.status == 1) {
		       	    alert("上传成功！");
		       	    $scope.slide.image = data.fileUrl;
		       	    
		       	    $scope.slideForm.$pristine = false;
		        }else {
		       	    alert(data.info);
		        }
		    });   
		}
		
	});
	app.filter('imgFilter', function(){
		return function(img){
			if (!img) {
				return 'admin/image/default.jpg';
			}else{
				return "/upload/"+img;
			}
		}
	});  
	
	$(function(){
	});
</script>
</body>
</@html>