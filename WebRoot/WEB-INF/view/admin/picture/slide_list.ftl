<#include "../../common/_home.ftl">
<@html>
<@head>
</@head>
<style>
body{color:#333;}
.distance{margin-top:20px;}
.move{
	float:left;
}
.caption{
	font-size: 18px;
	font-weight: 400;
	float:right;
}
thead{background:#f9f9f9;}
div, input, select, textarea, span, img, table, td, th, p, a, button, ul, li {
    border-radius: 0px !important;
    font-family:'Microsoft YaHei';
	}
	.returnl{position:absolute;right:20px;top:14px;}
	.breadcrumb{
		background:none;
		padding:0px;
		margin:0px;
		position:absolute;
		top:24px;
		left:130px;
	}
</style>
<body ng-app="app">
<div class="container-fluid" ng-controller="SlideController">
	<div class="row-fluid">
			<div class="span12">
				<h3 class="page-title">
					图片系统		
				</h3>
				<hr>
			</div>
	<ul class="breadcrumb">
		<li>
			<a>图片列表</a>
		</li>
	</ul>
	</div>
	<div class="clearfix"></div>
	<a class="btn blue move" href="admin/slide/slideInfo" data-toggle="modal"><i class="icon-plus"></i> 添加图片</a> <br>
	<!-- BEGIN PAGE BODY-->
 	<div class="row-fluid distance"> 
 		<div class="portlet-body">
 			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>图片名称</th>		
						<th>封面图片</th>		
						<th>创建时间</th>		
						<th>状态</th>		
						<th>操作</th>		
					</tr>
				</thead>
				<tr ng-repeat="slide in slides">
					<td ng-bind="slide.name"></td>
					<td style="width:120px;">
						<img ng-src="{{slide.image | imgFilter}}" class="img-polaroid" style="width:100px;height:50px;">
					</td>
					<td ng-bind="slide.create_time"></td>
					<td ng-bind="slide.status | statusFilter"></td>
					<td>
						<a class="btn middle" href="admin/slide/slideInfo?id={{slide.id}}"><i class="icon-pencil"></i> 编辑</a>
		    	  		<a class="btn middle" href="javascript:;" ng-click="delete(slide.id)"><i class="icon-search" style="margin-left:5px;"></i> 删除</a>
					</td>
				</tr>
			</table>
			<#include "../../common/_pagebar.ftl" />  
 		</div>
 	</div>
 	<!-- END PAGE BODY-->
	<div class="clearfix"></div>
</div>
<@plugin/>
<script type="text/javascript" src="/js/angular/angular.min.js"></script>
<script type="text/javascript" src="/js/angular/angular.extend.js"></script>
	<!-- END CORE PLUGINS -->
<script>
	var app = angular.module('app', []);
	app.controller('SlideController',function($scope,$http){
		$scope.gotoPage = function(pageNo){
			var pageNumber = pageNo ? pageNo : 1;
			
			$http.post("admin/slide/slideData?page_no="+ pageNumber).success(function(data){
				$scope.slides = angular.fromJson(data.rows);
				$scope.page = angular.formatPage(data.page);		
			});				
		}
		$scope.gotoPage(1);
		
		$scope.delete = function(id){
			if(!confirm("确定要删除当前图片吗？")){
				return;
			}
			$http.post("admin/slide/slideDelete?id="+id).success(function(result){
				if (result.status == 1) {
					alert("删除成功！");
					$scope.gotoPage($scope.page.pageNumber);
				}else{
					alert(result.info);
				}
			});
		};
	});
	app.filter('statusFilter', function(){
		return function (value) {
			if(value==1){
				return '显示';
			}
			return '隐藏';
		}
	}).filter('imgFilter', function(){
		return function(img){
			if (!img) {
				return 'img/system/default.jpg';
			}else{
				return "/upload/"+img;
			}
		}
	}); 
</script>
</body>
</@html>