<#include "../../common/_home.ftl">
<@html>
<@head>
	<link rel="stylesheet" type="text/css" href="/media/css/msg_preview.css">
	<script type="text/javascript" src="/js/angular/angular.min.js"></script>
	<script type="text/javascript" src="/js/angular/angular.extend.js"></script>
</@head>
    <style>
    body{color:#333;}
	div, input, select, textarea, span, img, table, td, th, p, a, button, ul, li {
    border-radius: 0px !important;
    font-family:'Microsoft YaHei';
	}
	.returnl{position:absolute;right:20px;top:14px;}
	.breadcrumb{
		background:none;
		padding:0px;
		margin:0px;
		position:absolute;
		top:24px;
		left:130px;
	}
    </style>
<body ng-app="app">
<div class="container-fluid" ng-controller="IntroController">
	<div class="row-fluid">
			<div class="span12">
				<h3 class="page-title">公司信息</h3>
				<hr>
			</div>
	</div>
	<ul class="breadcrumb">
					<li>
						<a href="#">联系我们</a>
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="javascript:void()">编辑</a>
					</li>
		     </ul>
	<div class="clearfix"></div>
	<!-- BEGIN PAGE BODY-->
 	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN SAMPLE FORM PORTLET-->   

				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					<form id="form" name="introForm" class="form-horizontal"  ng-submit="save();">
						
						<div class="control-group">
							<label class="control-label">标题<span class="required">*</span> ：</label>
								<div class="controls">
								<input type="text" ng-model="data.title"  placeholder="标题为自动生成"readonly/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">地址<span class="required">*</span> ：</label>
								<div class="controls">
								<input type="text" name="addr" ng-model="data.addr"  placeholder="地址" ng-maxlength="50"/>
								<label style="color:red;" ng-if="form.addr.$error.maxlength">地址长度超过50位</label>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">电话<span class="required">*</span> ：</label>
								<div class="controls">
								<input type="text" name="tel" placeholder="请输入联系方式" ng-maxlength="13" ng-model="data.tel" required/>
								<label style="color:red;" ng-if="form.tel.$error.maxlength">联系电话长度超过13位</label>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">传真 ：</label>
								<div class="controls">
								<input type="text" ng-model="data.num"  placeholder="请输入传真"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">E-mail<span class="required">*</span> ：</label>
								<div class="controls">
								<input type="email" ng-model="data.email"  placeholder="请输入邮箱"/>
							</div>
						</div>
						      <button type="submit" class="btn blue"  style="margin-right:10px;margin-left:160px;">保存</button>
						
					  </form>
					<!-- END FORM-->  

			</div>

			<!-- END SAMPLE FORM PORTLET-->

		</div>

	</div>
 	<!-- END PAGE BODY-->
	<div class="clearfix"></div>
</div>
	<@plugin/>
	
	<!-- END CORE PLUGINS -->
<script text/javascript>
	var app = angular.module('app',[]);
	
	app.controller('IntroController',function($scope, $http){
	<#if intro?exists>
		$scope.data=${intro};
	<#else>
		$scope.data={};
	</#if>
		
		$scope.save=function(){
			if ($scope.introForm.$pristine) {
				alert("请修改后再保存！");
				return;
			}
			if ($scope.introForm.$invalid) {
				alert("请输入正确的设置内容！");
				return;
			}
			
			jQuery.post("admin/intro/edit",angular.toModel($scope.data,"intro"),function(data){
				if(data.status == 1){
					alert("保存成功!");
				}else{
					alert(data.info);
				}			
			});		 
		};
		
	});
	
</script>
</body>
</@html>