<#include "../../common/_home.ftl">
<@html>
<@head>
	<link rel="stylesheet" type="text/css" href="/media/css/msg_preview.css">
	<script type="text/javascript" src="/js/angular/angular.min.js"></script>
	<script type="text/javascript" src="/js/angular/angular.extend.js"></script>
	<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.js"> </script>
	<script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"> </script>
	<script type="text/javascript" src="/js/angular/angular-ueditor.js"></script>
</@head>
    <style>
    body{color:#333;}
	div, input, select, textarea, span, img, table, td, th, p, a, button, ul, li {
    border-radius: 0px !important;
    font-family:'Microsoft YaHei';
	}
	.returnl{position:absolute;right:20px;top:14px;}
	.breadcrumb{
		background:none;
		padding:0px;
		margin:0px;
		position:absolute;
		top:24px;
		left:130px;
	}
    </style>
<body ng-app="app">
<div class="container-fluid" ng-controller="ProController">
	<div class="row-fluid">
			<div class="span12">
				<h3 class="page-title">新闻系统</h3>
				<hr>
				<div class="returnl">
                    <a class="btn blue" href="admin/product/proList/${pageNo?default(1)}"><i class="icon-reorder"></i>返回列表</a>
                </div>
			</div>
	</div>
	<ul class="breadcrumb">
					<li>
						<a href="admin/brand/list">新闻列表</a>
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="javascript:void()">文章编辑</a>
					</li>
		     </ul>
	<div class="clearfix"></div>
	<!-- BEGIN PAGE BODY-->
 	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN SAMPLE FORM PORTLET-->   

				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					<form id="form" name="proForm" class="form-horizontal"  ng-submit="save();">
						
						<div class="control-group">
							<label class="control-label">所属分类<span class="required">*</span> ：</label>
							<div class="controls">
								<select id="type" ng-options="t.name for t in types" ng-model="type" required>
									<option value="">==请选择==</option>
								</select>
								&nbsp;&nbsp;&nbsp;<a href="admin/product/typeList"> + 新增类别</a>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">产品名称<span class="required">*</span> ：</label>
								<div class="controls">
								<input type="text" name="proName" ng-model="pro.name" required ng-maxlength="30">
								<label style="color:red;" ng-if="proForm.proName.$error.maxlength">品牌名称长度超过30位</label>
							</div>
						</div>
						<div class="control-group">
								<label class="control-label" for="title">产品图片<span class="required">*</span> ：</label>
								<div class="controls">
										<div class="frm_input_box" style="width:200px;">
												<div class="upload_area">
													<a ng-file-select="onFileSelect($files)" class="upload_access" accept="image/*">上传</a>
													<label>建议尺寸：宽700 * 高400</label>
												</div>
												<p class="js_cover upload_preview">
													<img id="logo_id" ng-src="{{pro.image | imgFilter}}" class="img-polaroid" >	
												</p>
										</div>
								</div>
						  </div>
						  <div class="control-group">
							<label class="control-label">产品摘要<span class="required">*</span> ：</label>
							<div class="controls">
							  <textarea name="proContent" placeholder="产品简介" type="text" ng-model="pro.cummary" ng-maxlength="120" required></textarea>
							  <label style="color:red;" ng-if="proForm.proContent.$error.maxlength">文章摘要超过120字符</label>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">状态<span class="required">*</span> ：</label>
							<div class="controls">
								<select ng-model="pro.status">
									<option value="0">隐藏</option>
									<option value="1">显示</option>
								</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">产品详情<span class="required">*</span> ：</label>
								<div class="controls">
								<div style="height:400px" class="ueditor" config="config" ng-model="pro.content" ></div>
							</div>
						</div>
						      <button type="submit" class="btn blue"  style="margin-right:10px;margin-left:160px;">保存</button>
						       <a href="admin/product/proList" class="btn">返回</a>
						
					  </form>
					<!-- END FORM-->  

			</div>

			<!-- END SAMPLE FORM PORTLET-->

		</div>

	</div>
 	<!-- END PAGE BODY-->
	<div class="clearfix"></div>
</div>
	<@plugin/>
	<script type="text/javascript" src="/media/js/app.js" ></script>
	<script type="text/javascript" src="/js/angular/angular-file-upload.min.js"></script>

	<!-- END CORE PLUGINS -->
<script text/javascript>
	var app = angular.module('app',['ngFileUpload','ng.ueditor']);
	app.controller('ProController',function($scope, $http, $upload){
		<#if proInfo?exists>
		$scope.pro = ${proInfo};
		<#else>
		$scope.pro = {status:1};
		</#if>
		$scope.config={
			toolbars:[['bold','italic','underline','forecolor','backcolor','|',
      		  'fontfamily','fontsize','|',
      		  'removeformat','formatmatch','pasteplain','|',
        		'attachment','|',
       			'source','link','unlink','|',
        		'insertcode']]
			//focus时自动清空初始化时的内容
            ,autoClearinitialContent:true
		};
		$http.post("admin/product/getType").success(function(result){
			$scope.types = result;
			
			angular.forEach($scope.types, function(type){
				if (type.id == $scope.pro.type_id) {
					$scope.type = type;
				}
			});
		});
		$scope.save=function(){
			if ($scope.proForm.$pristine) {
				alert("请修改后再保存！");
				return;
			}
			if ($scope.proForm.$invalid) {
				alert("请输入正确的设置内容！");
				return;
			}
			
			$scope.pro.type_id = $scope.type.id;
			
			jQuery.post("admin/product/proEdit",angular.toModel($scope.pro,"pdt"),function(data){
				
				if(data.status == 1){
					alert("保存成功!");
					if(!$scope.pro.id){
						$scope.pro.id = data.id;
					}
				}else{
					alert(data.info);
				}			
			});		 
		};	
		
		$scope.onFileSelect = function(files){
			var file = files[0];
			if (file == null) return;
			
			
			var pageContent = $(document.body);
			App.blockUI(pageContent, false);
			
			$scope.upload = $upload.upload({
		        url: "/admin/product/uploadImg?key=pro&path=product", 
		        file: file, 
		        fileName: file.name 
		    }).progress(function(evt) {        
		    }).success(function(data, status, headers, config) {        
		    	
		    	App.unblockUI(pageContent);
		        
		        if (data.status == 1) {
		       	    alert("上传成功！");
		       	    $scope.pro.image = data.fileUrl;
		       	    
		       	    $scope.proForm.$pristine = false;
		        }else {
		       	    alert(data.info);
		        }
		    });   
		}
		
	});
	app.filter('imgFilter', function(){
		return function(img){
			if (!img) {
				return 'admin/image/default.jpg';
			}else{
				return "/upload/"+img;
			}
		}
	});  
	
	$(function(){
	});
</script>
</body>
</@html>