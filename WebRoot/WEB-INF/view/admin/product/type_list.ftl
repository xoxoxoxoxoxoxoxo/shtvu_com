<#include "../../common/_home.ftl">
<@html>
<@head>
	<link rel="stylesheet" type="text/css" href="/admin/css/glyphicons.css">
	<link rel="stylesheet" type="text/css" href="/admin/css/datepicker.css" />
</@head>
<style>
body{color:#333;}
.distance{margin-top:20px;}
.move{
	float:left;
}
.caption{
	font-size: 18px;
	font-weight: 400;
	float:right;
}
thead{background:#f9f9f9;}
div, input, select, textarea, span, img, table, td, th, p, a, button, ul, li {
    border-radius: 0px !important;
    font-family:'Microsoft YaHei';
	}
	.returnl{position:absolute;right:20px;top:14px;}
	.breadcrumb{
		background:none;
		padding:0px;
		margin:0px;
		position:absolute;
		top:24px;
		left:130px;
	}
</style>
<body ng-app="app">
<div class="container-fluid" ng-controller="TypeController">
	<div class="row-fluid">
			<div class="span12">
				<h3 class="page-title">
					产品系统		
				</h3>
				<hr>
			</div>
	<ul class="breadcrumb">
		<li>
			<a>类别管理</a>
		<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="javascript:void()">编辑</a>
		</li>
	</ul>
	</div>
	<div class="clearfix"></div>
	<a class="btn blue move" href="#typeAdd" data-toggle="modal"><i class="icon-plus"></i> 新增类别</a> <br>
	<!-- BEGIN PAGE BODY-->
 	<div class="row-fluid distance"> 
 		<div class="portlet-body">
 			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>排序</th>		
						<th>类别名称</th>		
						<th>操作</th>		
					</tr>
				</thead>
				<tr ng-repeat="t in types">
					<td ng-bind="$index+1"></td>
					<td ng-bind="t.name"></td>
					<td >
					<a class="btn middle" href="#typeAdd" data-toggle="modal" ng-click="getType(t.id)"><i class="icon-pencil"></i> 编辑</a>
					<a class="btn middle" href="javascript:;" ng-click="delete(t.id)"><i class="icon-search" style="margin-left:5px;"></i> 删除</a>
					</td>
				</tr>
			</table>
			<#include "../../common/_pagebar.ftl" />  
 		</div>
 	</div>
	<div id="typeAdd" class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h3 id="myModalLabel">
						新增类别
					</h3>
			</div>
			<div class="modal-body">
				<div class="portlet-body">	
					<form id="form" name="contForm" class="form-horizontal"  ng-submit="save();">
						<div class="control-group">
							<label class="control-label">类别名称<span class="required">*</span> ：</label>
								<div class="controls">
								<input type="text" ng-model="type.name"  placeholder="类别名称"/>
							</div>
						</div>
						 <button type="submit" class="btn blue"  style="margin-right:10px;margin-left:160px;">保存</button>
					</form>				
				</div>	
			</div>
 	<!-- END PAGE BODY-->
	<div class="clearfix"></div>
</div>
<@plugin/>
<script type="text/javascript" src="/js/angular/angular.min.js"></script>
<script type="text/javascript" src="/js/angular/angular.extend.js"></script>
	<!-- END CORE PLUGINS -->
<script>
	var app = angular.module('app', []);
	app.controller('TypeController',function($scope,$http){
	$scope.gotoPage = function(pageNo){
			var pageNumber = pageNo ? pageNo : 1;
			
			$http.post("admin/product/typeData?page_no="+ pageNumber).success(function(data){
				$scope.types = angular.fromJson(data.rows);
				$scope.page = angular.formatPage(data.page);		
			});				
		}
		$scope.gotoPage(1);
	
	$scope.save=function(){
			if ($scope.contForm.$pristine) {
				alert("请修改后再保存！");
				return;
			}
			if ($scope.contForm.$invalid) {
				alert("请输入正确的设置内容！");
				return;
			}
			
			jQuery.post("admin/product/typeEdit",angular.toModel($scope.type,"type"),function(data){
				if(data.status == 1){
					alert("保存成功!");
					if(!$scope.type.id){
						$scope.type.id = data.id;
					}
					window.location.href="admin/product/typeList";
				}else{
					alert(data.info);
				}			
			});		 
		};
		$scope.delete = function(id){
			if(!confirm("确定要删除当前类别吗？")){
				return;
			}
			$http.post("admin/product/deleteType/"+id).success(function(result){
				if (result.status == 1) {
					alert("删除成功！");
					$scope.gotoPage($scope.page.pageNumber);
				}else{
					alert(result.info);
				}
			});
		};
		$scope.getType = function(id){
			$http.post("admin/product/typeInfo?id="+id).success(function(data){
				$scope.type=angular.fromJson(data.type);
			});
		};
		
	});
</script>
</body>
</@html>