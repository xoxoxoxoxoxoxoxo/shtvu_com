<#include "../../common/_home.ftl">
<@html>
<@head>
	<link rel="stylesheet" type="text/css" href="/media/css/msg_preview.css">
	<script type="text/javascript" src="/js/angular/angular.min.js"></script>
	<script type="text/javascript" src="/js/angular/angular.extend.js"></script>
	<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.js"> </script>
	<script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"> </script>
	<script type="text/javascript" src="/js/angular/angular-ueditor.js"></script>
</@head>
    <style>
    body{color:#333;}
	div, input, select, textarea, span, img, table, td, th, p, a, button, ul, li {
    border-radius: 0px !important;
    font-family:'Microsoft YaHei';
	}
	.returnl{position:absolute;right:20px;top:14px;}
	.breadcrumb{
		background:none;
		padding:0px;
		margin:0px;
		position:absolute;
		top:24px;
		left:130px;
	}
    </style>
<body ng-app="app">
<div class="container-fluid" ng-controller="contController">
	<div class="row-fluid">
			<div class="span12">
				<h3 class="page-title">公司信息</h3>
				<hr>
			</div>
	</div>
	<ul class="breadcrumb">
					<li>
						<a href="#">公司介绍</a>
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="javascript:void()">编辑</a>
					</li>
		     </ul>
	<div class="clearfix"></div>
	<!-- BEGIN PAGE BODY-->
 	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN SAMPLE FORM PORTLET-->   

				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					<form id="form" name="contForm" class="form-horizontal"  ng-submit="save();">
						
						<div class="control-group">
							<label class="control-label">标题<span class="required">*</span> ：</label>
								<div class="controls">
								<input type="text" ng-model="data.title"  placeholder="标题为自动生成"readonly/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">详细内容<span class="required">*</span> ：</label>
								<div class="controls">
								<div style="height:400px" class="ueditor" config="config" ng-model="data.content" ></div>
							</div>
						</div>
						      <button type="submit" class="btn blue"  style="margin-right:10px;margin-left:160px;">保存</button>
						
					  </form>
					<!-- END FORM-->  

			</div>

			<!-- END SAMPLE FORM PORTLET-->

		</div>

	</div>
 	<!-- END PAGE BODY-->
	<div class="clearfix"></div>
</div>
	<@plugin/>
	
	<!-- END CORE PLUGINS -->
<script text/javascript>
	var app = angular.module('app',['ng.ueditor']);
	
	app.controller('contController',function($scope, $http){
	<#if cont?exists>
		$scope.data=${cont};
	<#else>
		$scope.data={};
	</#if>
		
		$scope.config={
			toolbars:[['bold','italic','underline','forecolor','backcolor','|',
      		  'fontfamily','fontsize','|',
      		  'removeformat','formatmatch','pasteplain','|',
        		'simpleupload','attachment','|',
       			'source','link','unlink','|',
        		'insertcode']]
			//focus时自动清空初始化时的内容
            ,autoClearinitialContent:true
		};
		
		$scope.save=function(){
			if ($scope.contForm.$pristine) {
				alert("请修改后再保存！");
				return;
			}
			if ($scope.contForm.$invalid) {
				alert("请输入正确的设置内容！");
				return;
			}
			
			jQuery.post("admin/cont/edit",angular.toModel($scope.data,"cont"),function(data){
				if(data.status == 1){
					alert("保存成功!");
					if(!$scope.data.id){
						$scope.data.id = data.id;
					}
				}else{
					alert(data.info);
				}			
			});		 
		};
		
	});
	
</script>
</body>
</@html>