<#macro html>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->

<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
	<#nested>
</html>
</#macro>

<#macro head>
<head>    
	<meta charset="utf-8" />
	<title>网站后台管理</title>	
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"> 
     <base href="${basePath}">
    <link rel="stylesheet" type="text/css" href="/admin/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/admin/css/bootstrap-responsive.min.css">
	<link rel="stylesheet" type="text/css" href="/admin/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/admin/css/style-metro.css">
	<link rel="stylesheet" type="text/css" href="/admin/css/style.css">
	<link rel="stylesheet" type="text/css" href="/admin/css/style-responsive.css">	
	<link rel="stylesheet" type="text/css" href="/admin/css/uniform.default.css">
    <#nested>
</head>
</#macro>


<#macro plugin>
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

	<!-- BEGIN CORE PLUGINS -->
	<script type="text/javascript" src="/admin/js/jquery-1.10.1.min.js" ></script>	
	<script type="text/javascript" src="/admin/js/jquery-migrate-1.2.1.min.js" ></script>
	<script type="text/javascript" src="/admin/js/jquery-ui-1.10.1.custom.min.js" ></script>
	<script type="text/javascript" src="/admin/js/bootstrap.min.js" ></script>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="/admin/js/excanvas.min.js" ></script>
	<script type="text/javascript" src="/admin/js/respond.min.js" ></script>
	<![endif]-->   
	<script type="text/javascript" src="/js/jquery.cookie.js" ></script>
	<script type="text/javascript" src="/admin/js/jquery.slimscroll.min.js" ></script>
	<script type="text/javascript" src="/admin/js/jquery.blockui.min.js" ></script>
	<script type="text/javascript" src="/admin/js/jquery.uniform.min.js" ></script>	
	<!-- END CORE PLUGINS -->
</#macro>

<#macro direction_css>
<style>
	.page-title
   {
      margin-top:18px;
      margin-bottom:15px;
   }
   .breadcrumb 
   {
    margin: 0px 0px 4px;
    font-size:14px;
   }
   .span12 label
   {
      display:inline;
      font-size:18px;
   }
   .fontNum
   {
      font-weight:bold;
   }
   .row-fluid 
   {
      margin-top:4px;
      margin-bottom:5px;
   }
   .row-fluid .last
   {
      margin-bottom:0px;
   }
   .imgEmpty
   {
      margin-bottom:10px;
      margin-top:5px;
   }
   .imgHeight
   {
      margin-bottom:18px;
   }
   .repeat
   {
      margin:15px;
   }
   .imgRepeat
   {
      margin-top:15px;
   }
  </style>
</#macro>