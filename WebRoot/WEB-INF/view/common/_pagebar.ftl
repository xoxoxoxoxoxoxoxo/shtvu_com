<div class="row-fluid" ng-if="page.show">
        <div class="span6">
                <div class="pagination-total">
                        第{{page.firstRow}} 至 {{page.lastRow}} 条 / 共 {{page.totalRow}} 条
                </div>
        </div>
        <div class="span6">
                <div class="pagination pagination-right">
                        <ul>
                                <li ng-if="page.isFirstPage">
                                        <span class="prev disabled">
                                                上页
                                        </span>
                                </li>
                                <li ng-if="!page.isFirstPage">
                                        <a href="javascript:;" ng-click="gotoPage(page.pageNumber-1)" class="prev">上页</a>
                                </li>
                                <li ng-if="page.pageNumber>8">
                                        <a href="javascript:;" ng-click="gotoPage(1)" class="prev">1</a>
                                </li>
                                <li ng-if="page.pageNumber>8">
                                        <a href="javascript:;" ng-click="gotoPage(2)" class="prev">2</a>
                                </li>
                                <li ng-if="page.pageNumber>8">
                                        <span class="gap">
                                                …
                                        </span>
                                </li>
                                <li ng-repeat="item in page.items">
                                        <span ng-if="page.pageNumber==item" class="active">
                                                {{item}}
                                        </span>
                                        <a ng-if="page.pageNumber!=item" href="javascript:;" ng-click="gotoPage(item)" class="prev">{{item}}</a>
                                </li>
                                <li ng-if="page.totalPage-page.pageNumber>=8">
                                        <span class="gap">
                                                …
                                        </span>
                                </li>
                                <li ng-if="page.totalPage-page.pageNumber>=8">
                                        <a href="javascript:;" ng-click="gotoPage(page.totalPage-1)" class="prev">{{page.totalPage-1}}</a>
                                </li>
                                <li ng-if="page.totalPage-page.pageNumber>=8">
                                        <a href="javascript:;" ng-click="gotoPage(page.totalPage)" class="prev">{{page.totalPage}}</a>
                                </li>
                                <li ng-if="page.isLastPage">
                                        <span class="next disabled">
                                                下页
                                        </span>
                                </li>
                                <li ng-if="!page.isLastPage">
                                        <a href="javascript:;" ng-click="gotoPage(page.pageNumber+1)" class="next" rel="next">下页</a>
                                </li>
                        </ul>
                </div>
        </div>
</div>