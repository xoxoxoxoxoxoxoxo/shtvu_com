<#include "_newhome.ftl">
<@html>
<@head>
<style>
	.titleholder { 
		height: 345px;
		overflow: hidden;  
		} 
</style>
</@head>
<body class="boxed">
<!-- begin container -->
<div id="wrap">
<#include "_newhead.ftl" />	
    <!-- begin page title -->
    <section id="page-title">
    	<div class="container clearfix">
            <h1>${cont.title}</h1>
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="/">主页</a> &rsaquo;</li>
                    <li>${cont.title}</li>
                </ul>
            </nav>
        </div>
    </section>
    <!-- end page title -->
    <!-- begin content -->
<section id="content" class="container clearfix">
		<!-- begin our company -->
        <section>
            <div >
                <p>${cont.content}</p>
            </div>
            <div class="clear"></div>
        </section>
        <!-- begin our company -->
        
        <hr>
</section>
    <!-- end content -->  
    
<#include "_footer.ftl"/>
</div>
</body>
</@html>