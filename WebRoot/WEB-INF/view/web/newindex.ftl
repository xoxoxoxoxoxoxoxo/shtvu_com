<#include "_newhome.ftl">
<@html>
<@head>
<style>
	.titleholder { 
		height: 325px;
		overflow: hidden;  
		} 
	#aboutus{
		font-family:"Microsoft Yahei";
	}
</style>
</@head>
<body class="boxed" ng-app="app" ng-controller="IndexController">
<!-- begin container -->
<div id="wrap">
<#include "_newhead.ftl" />	
<#include "_lunbo.ftl" />	
<section id="content" class="container clearfix">
        <!-- begin intro -->
        <section class="intro">
        	<h1>我们是 <strong>XXX科技有限公司</strong>.欢迎您的访问！</h1>
        </section>
        <!-- end intro -->
        <hr>
        <!-- begin about us -->
        <section class="one-half">
            <h3>关于我们</h3>
            <div id="aboutus" class="titleholder" style="padding-right:5px;">${cont}</div>
            <a class="button generic" style="margin-top:18px;" href="/about">查看详情</a>
        </section>
        <!-- end about us -->
        
        <!-- end latest posts -->
        <section class="one-half column-last">
            <h3>新闻系统</h3>
            <!-- begin post carousel -->
            <ul class="post-carousel">
                <!-- begin first column -->
                <li>
                    <div class="entry" ng-repeat="new in newsFirst">
                        <div class="entry-date">
                            <div class="entry-day">{{new.day}}</div>
                            <div class="entry-month">{{new.month}}月</div>   
                        </div>
                        <div class="entry-body">
                            <h4 class="entry-title"><a href="/news/info?id={{new.id}}">{{new.title}}</h4>
                            <div class="entry-content" style="color:black;">
                                <p>{{new.summary}}&hellip;</p>
                            </div>
                        </div>
                    </div>
                    
                </li>
                <!-- end first column -->
                
                <!-- begin second column -->
                <li>
               		<div class="entry" ng-repeat="new in newsSecond">
                        <div class="entry-date">
                            <div class="entry-day">{{new.day}}</div>
                            <div class="entry-month">{{new.month}}月</div>   
                        </div>
                        <div class="entry-body">
                            <h4 class="entry-title"><a href="/news/info?id={{new.id}}">{{new.title}}</h4>
                            <div class="entry-content" style="color:black;">
                                <p>{{new.summary}}&hellip;<p>
                            </div>
                        </div>
                    </div>
                </li>
                <!-- end second column -->
            </ul>
            <!-- end post carousel -->
            
            <a class="button generic" href="/news/list">查看更多</a>
        </section>
        <!-- end latest posts -->
        
        <hr>
        
        <!-- begin latest projects -->
        <section>
        	<h3>产品展示</h3>
            <!-- begin project carousel -->
            <ul class="project-carousel project-list">
            	<!-- 插件分页 -->
                <#list pro as p>
                <li class="entry">
                    <a class="entry-image lightbox" href="/upload/${p.image}" title="${p.name}"><span class="overlay zoom"></span><img src="/upload/${p.image}" alt=""></a>
                    <a class="entry-meta" href="/product/info?id=${p.id}">
                        <h4 class="entry-title">${p.name}</h4>
                        <div class="entry-content">
                            <p>${p.type}</p>
                        </div>
                    </a>
                </li>
                </#list>
            </ul>
            
            <!-- end project carousel -->
        </section>
        <!-- end latest projects -->
        
        <hr>
        
    </section>
    <!-- end content -->  
    
<#include "_footer.ftl"/>

</div>
	<@plugin/>	
	<script type="text/javascript" src="/js/angular/angular.min.js"></script>
	<script type="text/javascript" src="/js/angular/angular.extend.js"></script>
	<script>
	var app = angular.module('app',[]);
	app.controller('IndexController',function($scope,$http){
		$scope.gotoPage = function(pageNo){
			var pageNumber = pageNo ? pageNo : 1;
			
			$http.post("/newsTop?page_no="+1).success(function(data){
				$scope.newsFirst = angular.fromJson(data.rows);
				$scope.page = angular.formatPage(data.page);		
			});				
			$http.post("/newsTop?page_no="+2).success(function(data){
				$scope.newsSecond = angular.fromJson(data.rows);
				$scope.page = angular.formatPage(data.page);		
			});				
		}
		$scope.gotoPage(1);
	});
	</script>
</body>
</@html>