<!-- begin header -->
    <header id="header" class="container clearfix">
    	<!-- begin logo -->
        <h1 id="logo"><a href=""><img src="/media/images/logo.png" alt="Exquiso"></a></h1>
        <!-- end logo -->
        
        <!-- begin navigation wrapper -->
        <div class="nav-wrap clearfix">
        
        <!-- begin navigation -->
        <nav id="nav">
            <ul id="navlist" class="clearfix">
                <li><a href="/">首页</a></li>
                <li><a href="/about">公司介绍</a></li>
                <li><a href="/news/list">新闻中心</a></li>
                <li><a href="/product/list">产品中心</a></li>
            </ul>
        </nav>
        <!-- end navigation -->
        
        </div>
        <!-- end navigation wrapper -->
    </header>
    <!-- end header -->