<#include "../_newhome.ftl">
<@html>
<@head>
<style>
	.titleholder { 
		height: 345px;
		overflow: hidden;  
		} 
</style>
</@head>
<body class="boxed">
<!-- begin container -->
<div id="wrap">
<#include "../_newhead.ftl" />	
    <!-- begin page title -->
    <section id="page-title">
    	<div class="container clearfix">
            <h1>新闻</h1>
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="/">主页</a> &rsaquo;</li>
                    <li>新闻  &rsaquo;</li>
                    <li>${newsInfo.title}</li>
                </ul>
            </nav>
        </div>
    </section>
    <!-- end page title -->
    <!-- begin content -->
    <section id="content" class="container clearfix">
    	<!-- begin main content -->
        <section id="main" class="blog-entry-list three-fourths">
			<article class="entry clearfix" >
				<div class="entry-date">
					<div class="entry-day">${newsInfo.day}</div>
					<div class="entry-month">${newsInfo.month}月</div>   
				</div>
				<div class="entry-body">
					<h2 class="entry-title"><a href="">${newsInfo.title}</a></h2>
			            <div class="entry-content">
			                 <p>${newsInfo.content}</p>
			            </div>
				</div>
			</article>
			<hr>
		</section>
	</section>
    <!-- end content --> 
<#include "../_footer.ftl"/>
</div>
</body>
</@html>