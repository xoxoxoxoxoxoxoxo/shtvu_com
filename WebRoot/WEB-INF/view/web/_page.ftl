<nav class="page-nav">
	<ul>
		<#if page.pageNumber gt 1>
			<li><a href="${ctx!}/${pageUrl!}/1"  alt="首页">首页</a></li>
			<li><a href="${ctx!}/${pageUrl!}/${page.pageNumber-1}" >上一页</a></li>
		<#else>
			<li><a href="javascript:void(0);" >首页</a></li>
			<li><a href="javascript:void(0);" >上一页</a></li>
		</#if>
		
		<#if page.totalPage == 0>
			<li><a href="javascript:void(0);">0</a></li>
		<#elseif page.totalPage lte 10>
			<#list 1..page.totalPage as a>
				<#if page.pageNumber = a>
					<li class="current">${a}</li>
				<#else>	
					<li><a href="${ctx!}/${pageUrl!}/${a}">${a}</a></li>
				</#if>	
			</#list>
		<#else>
			<#list (page.totalPage-10)..page.totalPage as a>
				<#if page.pageNumber = a>
					<li class="current">${a}</li>
				<#else>	
					<li><a href="${ctx!}/${pageUrl!}/${a}">${a}</a></li>
				</#if>	
			</#list>
		</#if>
		
		<#if page.pageNumber lt page.totalPage>
			<li><a href="${ctx!}/${pageUrl!}/${page.pageNumber+1}" >下一页</a></li>
			<li><a href="${ctx!}/${pageUrl!}/${page.totalPage}" >尾页</a></li>
		<#else>
			<li><a href="javascript:void(0);" >下一页</a></li>
			<li><a href="javascript:void(0);" >尾页</a></li>
		</#if>
	</ul>
</nav>