<#include "../_newhome.ftl">
<@html>
<@head>
<style>
	.titleholder { 
		height: 345px;
		overflow: hidden;  
		} 
</style>
</@head>
<body class="boxed">
<!-- begin container -->
<div id="wrap">
<#include "../_newhead.ftl" />	
    <!-- begin page title -->
    <section id="page-title">
    	<div class="container clearfix">
            <h1>产品</h1>
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="/">主页</a> &rsaquo;</li>
                    <li>产品</li>
                </ul>
            </nav>
        </div>
    </section>
    <!-- end page title -->
    <!-- begin content -->
    <section id="content" class="container clearfix">
    	<!-- begin main content -->
    	<!-- begin filter -->
        <div id="filter" class="option-set">
            <span>产品类别:</span>
            <ul>
                <li><a href="#" data-filter="*">全部</a></li>
                <#list typeName as type>
                <li><a href="#" data-filter=".${type.name}">${type.name}</a></li>
                </#list>
            </ul>
        </div>
        <!-- end filter -->
        <ul id="gallery" class="portfolio-grid project-list clearfix">
        	<#list pro as p>
            <li class="entry one-fourth ${p.type}">
                <a class="entry-image lightbox" href="/upload/${p.image}" title="${p.name}"><span class="overlay zoom"></span><img src="/upload/${p.image}" alt=""></a>
                <a class="entry-meta" href="/product/info?id=${p.id}">
                    <h4 class="entry-title">${p.name}</h4>
                    <div class="entry-content">
                        <p>${p.type}</p>
                    </div>
                </a>
            </li>
            </#list>
         </ul>    
	</section>
    <!-- end content --> 
<#include "../_footer.ftl"/>
</div>
</body>
</@html>