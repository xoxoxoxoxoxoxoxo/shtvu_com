<#include "../_newhome.ftl">
<@html>
<@head>
<style>
	.titleholder { 
		height: 345px;
		overflow: hidden;  
		} 
</style>
</@head>
<body class="boxed">
<!-- begin container -->
<div id="wrap">
<#include "../_newhead.ftl" />	
    <!-- begin page title -->
    <section id="page-title">
    	<div class="container clearfix">
            <h1>产品中心</h1>
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="/">主页</a> &rsaquo;</li>
                    <li>新闻  &rsaquo;</li>
                    <li>${proInfo.name}</li>
                </ul>
            </nav>
        </div>
    </section>
    <!-- end page title -->
    <!-- begin content -->
    <section id="content" class="container clearfix">
    	 <!-- begin project -->
        <section>
            <!-- begin project media -->
            <div class="three-fourths">
            	<a class="entry-image lightbox" href="/upload/${proInfo.image}" title="Image Title"><span class="overlay zoom"></span><img src="/upload/${proInfo.image}" alt=""></a>
            </div>
            <!-- end project media -->
            
            <!-- begin project description -->
            <div class="one-fourth column-last">
                <h3>产品简介</h3>
                <p>${proInfo.cummary}</p>
                <h3>产品类别</h3>
                <p>&mdash; ${proInfo.type}</p>
            </div>
            <!-- end project description -->
            <div class="clear"></div>
        </section>
        <hr>
        <!-- end project -->
        <div class="entry-body">
			<div class="entry-content">
				${proInfo.content}
			</div>
         </div>
	</section>
    <!-- end content --> 
<#include "../_footer.ftl"/>
</div>
</body>
</@html>