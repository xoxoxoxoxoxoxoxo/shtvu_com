<#macro html>
<!DOCTYPE html>
<!--[if IE 8]> <html class="ie8 no-js"> <![endif]-->
<!--[if IE 8]> <html class="ie8 no-js"> <![endif]--><html lang="en" class="no-js"><!--<![endif]-->
<#nested>
</html>
</#macro>

<#macro head>
<head>    
	<!-- begin meta -->
	<meta charset="utf-8" />
	<title>网站主页</title>	
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"> 
    <base href="${basePath}">
	<!-- begin CSS -->
	<link href="/media/style.css" type="text/css" rel="stylesheet" id="main-style">
	<link href="/media/css/responsive.css" type="text/css" rel="stylesheet">
	<!--[if IE]> <link href="css/ie.css" type="text/css" rel="stylesheet"> <![endif]-->
	<link href="/media/css/colors/blue.css" type="text/css" rel="stylesheet" id="color-style">
    <link href="/media/style-switcher/style-switcher.css" type="text/css" rel="stylesheet">
	<!-- end CSS -->
	
    <link href="/media/images/favicon.ico" type="image/x-icon" rel="shortcut icon">

	<!-- begin JS -->
    <script src="/media/js/jquery-1.8.2.min.js" type="text/javascript"></script> <!-- jQuery -->
    <script src="/media/js/ie.js" type="text/javascript"></script> <!-- IE detection -->
    <script src="/media/js/jquery.easing.1.3.js" type="text/javascript"></script> <!-- jQuery easing -->
	<script src="/media/js/modernizr.custom.js" type="text/javascript"></script> <!-- Modernizr -->
    <!--[if IE 8]>
    <script src="/media/js/respond.min.js" type="text/javascript"></script> 
    <script src="/media/js/selectivizr-min.js" type="text/javascript"></script> 
    <![endif]--> 
    <script src="/media/style-switcher/style-switcher.js" type="text/javascript"></script> <!-- style switcher -->
    <script src="/media/js/ddlevelsmenu.js" type="text/javascript"></script> <!-- drop-down menu -->
    <script type="text/javascript"> <!-- drop-down menu -->
        ddlevelsmenu.setup("nav", "topbar");
    </script>
    <script src="/media/js/tinynav.min.js" type="text/javascript"></script> <!-- tiny nav -->
    <script src="/media/js/jquery.validate.min.js" type="text/javascript"></script> <!-- form validation -->
    <script src="/media/js/jquery.flexslider-min.js" type="text/javascript"></script> <!-- slider -->
    <script src="/media/js/jquery.jcarousel.min.js" type="text/javascript"></script> <!-- carousel -->
    <script src="/media/js/jquery.isotope.min.js" type="text/javascript"></script> <!-- portfolio filter -->
    <script src="/media/js/jquery.ui.totop.min.js" type="text/javascript"></script> <!-- scroll to top -->
    <script src="/media/js/jquery.fitvids.js" type="text/javascript"></script> <!-- responsive video embeds -->
    <script src="/media/js/jquery.tweet.js" type="text/javascript"></script> <!-- Twitter widget -->
    <script type="text/javascript" src="/media/js/revslider.jquery.themepunch.plugins.min.js"></script> <!-- swipe gestures -->
    <script src="/media/js/jquery.tipsy.js" type="text/javascript"></script> <!-- tooltips -->
    <script src="/media/js/jquery.fancybox.pack.js" type="text/javascript"></script> <!-- lightbox -->
    <script src="/media/js/jquery.fancybox-media.js" type="text/javascript"></script> <!-- lightbox -->
    <script src="/media/js/froogaloop.min.js" type="text/javascript"></script> <!-- video manipulation -->
    <script src="/media/js/custom.js" type="text/javascript"></script> <!-- jQuery initialization -->
    <!-- end JS -->
	
    <#nested>
</head>
</#macro>


<#macro plugin>
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

	<!-- BEGIN CORE PLUGINS -->
	<!-- END CORE PLUGINS -->
</#macro>
