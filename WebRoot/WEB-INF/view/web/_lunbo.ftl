<!-- begin slider -->
    <section id="slider-home">
        <div id="flexslider-home" class="flex-container container">
            <div class="flexslider">
                <ul class="slides">
                    <#list slide as s>
                    <li>
                        <img src="/upload/${s.image}" alt="${s.name}">
                    </li>
                    </#list>
                </ul>
            </div>
        </div>
    </section>
    <!-- end slider -->