var c = document.getElementById('dragMe');
  	    
c.addEventListener('mousedown', mousedownHandler, false);  
c.addEventListener('touchstart', mousedownHandler, false);  
window.addEventListener('mousemove', mousemoveHandler, false);  
window.addEventListener('touchmove', mousemoveHandler, false);  
window.addEventListener('mouseup', mouseupHandler, false);  
window.addEventListener('touchend', mouseupHandler, false);

var mouseDown = false;
var mouseMove = false;

function mousedownHandler(e) {  
    mouseDown = true;  
    e.stopPropagation();
    if (e.cancelable) { e.preventDefault(); }   
    return false;  
};
function mousemoveHandler(e) {
	if (!mouseDown) { return true; } 
	mouseMove = true;
	
    var local = getLocalCoords(c, e);  
	
	c.style.left = (local.x - 30)+ 'px';
	c.style.top = (local.y - 30) + 'px';
	c.style.position = 'absolute';

    if (e.cancelable) { e.preventDefault(); }   
    return false;  
}
function mouseupHandler(e) { 
	
	if (mouseDown) {  
        mouseDown = false;  
        showMain();
        if (e.cancelable) { e.preventDefault(); }   
        return false;  
    } else {
//    	showButton();
    } 
    
    return true;  
};  

// 取得坐标
function getLocalCoords(elem, ev) {  
    
    if (ev.hasOwnProperty('changedTouches')) {  
            first = ev.changedTouches[0];  
            pageX = first.pageX;  
            pageY = first.pageY;  
        } else {  
            pageX = ev.pageX;  
            pageY = ev.pageY;  
        }  
  
        return { 'x': pageX, 'y': pageY };  
}

var main = document.getElementById('dragMen');

//显示菜单
var showMain = function() {
	if (!mouseMove) {
		c.style.display = 'none';
		main.style.display = 'block';
	}
	mouseMove = false;
	
	return true;
}

var showButton = function() {
	c.style.display = 'block';
	main.style.display = 'none';
	
	return true;
}

var changeTo = function(type){
	showButton();
	if (type == 'back') {
		window.history.back();
	}
}
