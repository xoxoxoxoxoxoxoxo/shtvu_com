'use strict';
// angular tools

var AT =  {};
AT.toModel = function(data,modelName){
	var result = {};	
	angular.forEach(data,function(value,key){		
		result[modelName+"."+key] = value;
	});
	return result;
};

AT.formatPage = function(page){	
	if(Object.prototype.toString.call(page) === "[object String]"){
		page = angular.fromJson(page);
	}
	if(page.totalRow){ 
		page.show = true;
		
		page.firstRow = (page.pageNumber-1) * page.pageSize + 1;
		page.lastRow = (page.pageNumber*page.pageSize > page.totalRow) ? page.totalRow : (page.pageNumber*page.pageSize);
		page.isFirstPage = (page.pageNumber == 1);
		page.startPage = (page.pageNumber> 4) ? page.pageNumber - 4 : 1;
		page.endPage = (page.totalPage-4> page.pageNumber) ? page.pageNumber+4 :page.totalPage; 
		page.items = [];
		for(var i=page.startPage;i<=page.endPage;i++){
			page.items.push(i);
		}		
		page.isLastPage = (page.pageNumber == page.totalPage);
	}	
	return page;
};
angular.extend(angular,AT);