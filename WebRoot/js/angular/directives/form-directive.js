'use strict';  
  
var formDirectives = angular.module('formDirectives', []);  
 
formDirectives.directive('time',function(){
	return {
		restrict : 'EA',
		replace : true,
		require: '?ngModel',
		template : '<div class="input-append bootstrap-timepicker-component">'
					+'<input class="m-wrap m-ctrl-small timepicker-default" type="text" style="width:100px"/>'
					+'<span class="add-on"><i class="icon-time"></i></span>'
					+'</div>',
		link : function(scope, element, attrs,ngModel){
			var box = jQuery(element).find("input:first");
			box.timepicker({
				showMeridian:false
			});
			if(!ngModel) return;
			ngModel.$render = function(){
				var value=ngModel.$viewValue || '';				
				box.timepicker('setTime',value);
			};
			box.timepicker().on('changeTime.timepicker', function(e) {
				scope.$apply(function(){
					ngModel.$setViewValue(e.time.value);
				});
			});
		}
	}
});

formDirectives.directive('date',function(){
	return {
		restrict : 'EA',
		replace : true,
		require: '?ngModel',
		scope:{format:'@',placeholder:'@'},
		template : '<div class="input-append date">'
					+'<input type="text" placeholder={{placeholder}} class="m-wrap m-ctrl-medium date-picker"  style="width:100px">'
					+'<span class="add-on"><i class="icon-calendar"></i></span>'
					+'</div>',
		link : function(scope, element, attrs,ngModel){		
			if(!ngModel) return;
            
			if (scope.format) jQuery(element).datepicker({format:scope.format});
			
			ngModel.$render = function() {
            	var value = ngModel.$viewValue || '';
            	if(!value){
            		return;
            	}
            	if(value.length>10){
            		value=value.substr(0,10);
            	}
            	
            	jQuery(element).datepicker("update",value);	
    		};
    		
    		jQuery(element).datepicker().on('changeDate', function(ev){
    			scope.$apply(function(){
    				ngModel.$setViewValue(jQuery(element).datepicker("getFormattedDate"));
    				jQuery(element).datepicker("hide");
    			});
    		});
		}
	};
});

formDirectives.directive('datetime',function(){
	return {
		restrict : 'EA',
		replace : true,
		require: '?ngModel',
		template : '<div class="input-append date form_datetime">'
						+'<input id="yd_time" size="16" type="text" readonly class="m-wrap">'
						+'<span class="add-on"><i class="icon-calendar"></i></span>'
				   +'</div>',
			link : function(scope, element, attrs,ngModel){		
				if(!ngModel) return;
				ngModel.$render = function() {
					
					jQuery(element).datetimepicker({
						  language: 'zh_CH',
			              pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
			        });
					
					var value = ngModel.$viewValue || '';
					if(!value){
						return;
					}
					if(value.length>10){
						value=value.substr(0,16);
					}
						
					jQuery(element).datetimepicker("update",value);	
				};
				jQuery(element).on('changeDate', function(e) {
					scope.$apply(function(){
	    				//ngModel.$setViewValue(jQuery(element).datetimepicker("getFormattedDate"));
						ngModel.$setViewValue(jQuery("#yd_time").val());
	    				jQuery(element).datetimepicker("hide");
	    			});
				});
		}
	};
});

formDirectives.directive('week',function(){
	return {			
            restrict: 'EA',
            replace : true,
            require: '?ngModel',
            template : '<div class="week"><p>一</p><p>二</p><p>三</p><p>四</p><p>五</p><p>六</p><p>日</p><label>[绿色选中]</label></div>',
            link: function(scope, element, attrs,ngModel) {		
            	 if(!ngModel) return;             	
            	ngModel.$render = function() {
            		var value = ngModel.$viewValue || '';
            		if(!value){
            			jQuery(element).find("p").toggleClass("checked",false);
            			return;
            		}else{
            			var days = value.split(",");
            			for(var i=0;i<days.length;i++){
            				jQuery(element).find("p:contains(" + days[i] + ")").toggleClass("checked",true);
            			}
            		}
    			};
				//绑定单击事件
				jQuery(element).on("click","p",function(){
					jQuery(this).toggleClass("checked");
					scope.$apply(read);
				});			
				  
			    // 将数据写入model
			    function read() {			    
			    	var days="";
			    	jQuery(element).find("p.checked").each(function(){
			    		if(jQuery(this).hasClass("checked")){
			    			var day = jQuery(this).text();
			    			days += (days=="") ?  day : ","+day;
			    		}
			    	});
			      	ngModel.$setViewValue(days);			     
			    };
            }
	};
});

formDirectives.directive('toggle',function(){
	return {
        restrict: 'E',
        replace : true,
        require: '?ngModel',
        template : '<div class="basic-toggle-button">'
					+'<input type="checkbox" class="toggle"/>'
					+'</div>',
		link: function(scope, element, attrs,ngModel) {
			jQuery(element).toggleButtons({
				onChange:function(el,value){					
					scope.$apply(function(){
						ngModel.$setViewValue((value)?1:0);
					});
				}
			});
			if(!ngModel) return;
			ngModel.$render = function(){
				var value=ngModel.$viewValue;					
				var booleanValue = (value) ? true : false;
				jQuery(element).toggleButtons('setState',booleanValue);
			};
		}
	};
});


formDirectives.directive('daterange',function(){
	return {
        restrict: 'EA',
        replace : true,
        require: '?ngModel',
        template : '<div class="input-append form_datetime">'
        	+'<div class="form-date-range" style="display:inline;height:24px;">'
			+'<input type="text" class="m-wrap" readonly="" placeholder="开始日期 至 结束日期"/>'
			+'<span class="add-on"><i class="icon-calendar"></i></span>'
			+'</div>'
			+'<span class="add-on"><i class="icon-remove" onclick="clean();"></i></span>'
			+'</div>',
		link: function(scope, element, attrs,ngModel) {
			jQuery(element).find('.form-date-range').daterangepicker({
				ranges: {
		        	'今天': ['today', 'today'],
		         	'昨天': ['yesterday', 'yesterday'],
		        	'最近7天': [Date.today().add({ days: -6 }), 'today'],
		            '本月': [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()],
		            '上月': [Date.today().moveToFirstDayOfMonth().add({
		            		months: -1
		            		}), Date.today().moveToFirstDayOfMonth().add({
		            		days: -1
		                    })]
				},
				format: 'yyyy-MM-dd',
				startDate: Date.today().add({days: -29}),
				endDate: Date.today(),
			},
			function (start, end) {
				$(element).find('.form-date-range input:text').val(start.toString('yyyy-MM-dd') + ' 至 ' + end.toString('yyyy-MM-dd'));
				ngModel.$setViewValue([start.toString('yyyy-MM-dd'),end.toString('yyyy-MM-dd')]);
			});
			jQuery(element).find('.icon-remove').click(function (){
				$(element).find('.form-date-range input:text').val("");
				ngModel.$setViewValue([]);
			});
		}
	};
});

//多选下拉列表扩展
formDirectives.directive('multiple', function(){
	return {
        restrict: 'A',
        replace : true,
//        require: '?ngModel',
//        template : '<div class="basic-toggle-button">'
//					+'<input type="checkbox" class="toggle"/>'
//					+'</div>',
		link: function(scope, element, attrs,ngModel) {
//			jQuery(element).multiSelect();
		}
	};
	
});
