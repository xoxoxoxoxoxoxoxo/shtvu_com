package com.feng.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SimpleTimeZone;

/**
 * 数据类型转换类.
 * @author yjs
 *
 */
public class Convert {
	public static final String DATE_FORMAT_YMD = "yyyy-MM-dd";
	public static final String DATE_FORMAT_YMDHS = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_FILENAME = "yyyy_MM_dd_HH_mm_ss";
	
	
	public static String getNameByTimes(double time){
		String name = "";
		if(time < 1){
			name = (int)time*60 + "分钟";
		}else if(time < 24){
			name = (int)time + "小时";
		}else if(time < 24*30){
			name = (int)time/24 + "天";
		}else{
			name = (int)time/24/30 + "月";
		}
		return name;
	}
	//根据日期获取年月
	public static int getYearMonth(Date day){
		Calendar cal = Calendar.getInstance();
		cal.setTime(day);
		return cal.get(Calendar.YEAR)*100+(cal.get(Calendar.MONTH)+1);
	}
	//根据日期获取年月日
	public static int getYearMonthDay(Date day){
		Calendar cal = Calendar.getInstance();
		cal.setTime(day);
		return cal.get(Calendar.YEAR)*10000 + (cal.get(Calendar.MONTH)+1)*100 + cal.get(Calendar.DAY_OF_MONTH);
	}
	
	//返回对象属性值组成的map.
	@SuppressWarnings("unchecked")
	public static Map getMap(Object src) throws Exception{
		Map result = new HashMap();
		if(src == null){
			return result;
		}
		Class cls = src.getClass();			
		Field[] fields = cls.getDeclaredFields();		
		for(int i=0;i<fields.length;i++){
			Field field = fields[i];
			field.setAccessible(true);
			Object value = field.get(src);
			if(value instanceof Date){
				result.put(field.getName(), Convert.getDateString((Date)value));
			}else{
				result.put(field.getName(), value);
			}			
		}
		return result;
	}	
	
	public static Map formatMapValue(Map map) throws Exception{
		Iterator it = map.keySet().iterator();
		while(it.hasNext()){
			Object key = it.next();
			Object value = map.get(key);
			if(value == null){
				map.put(key, "");
				continue;
			}
			if(value instanceof Date){
				map.put(key, Convert.getDateString((Date)value));
			}else if(value instanceof Boolean){
				map.put(key, (Boolean)value ? 1 : 0);
			}else{			
				map.put(key, value);
			}
		}
		return map;
	}
	
	public static boolean getBoolean(Object src) throws Exception
	{
		if(src==null)		
			throw new Exception("转换对象不能为空");		
		try
		{		
			if(src instanceof Boolean)
				return ((Boolean)src).booleanValue();	
			else
			{
				String str = getString(src);
				if(str.equals("1")||str.toLowerCase().equals("true")
						||str.toLowerCase().equals("y")||str.toLowerCase().equals("yes"))
					return true;
				else
					return false;
			}
		}
		catch(Exception e)
		{
			throw new Exception("转换对象为boolean类型发生错误,原因："+e.getMessage(),e);
		}
	}
	/**
	 * 返回该对象的boolean值.
	 * @param src
	 * @param def 对象为空时的返回值.
	 * @return
	 * @throws Exception
	 */
	public static boolean getBoolean(Object src,boolean def)
	{
		try
		{
			if(src!=null)
			{
				return getBoolean(src);				
			}
			else
				return def;	
		}
		catch(Exception e)
		{
			return def;
		}
	}
	/**
	 * 返回对象的字符串型值.
	 * @param src
	 * @return
	 * @throws Exception
	 */
	public static String getString(Object src) throws Exception
	{
		if(src==null)
		{
			return null;
		}
		try
		{
			if(src instanceof Date)
				return getDateString((Date)src);
			else
			return src.toString();
		}
		catch(Exception e)
		{
			throw new Exception("转换对象对String类型时出错，原因："+e.getMessage(),e);
		}
				
	}
	/**
	 * 返回对象的字符串型值.
	 * @param src
	 * @param def 对象为空时的返回值.
	 * @return
	 * @throws Exception
	 */
	public static String getString(Object src,String def)
	{
		try
		{
			if(src==null)
				return def;
			else
				return src.toString();
		}
		catch(Exception e)
		{
			return def;
		}		
	}
	private static String getNumberString(String src){
		String numString = "+-0123456789.";
		StringBuilder  sb = new StringBuilder();
		if(src == null || src.equals("")){			
		}else{
			char[] charArr = src.toCharArray();
			for(int i = 0; i<charArr.length;i++){
				char c = charArr[i];
				if(numString.contains(String.valueOf(c))){
					sb.append(c);
				}else{
					break;
				}
			}
		}		
		return sb.toString();
		
		
	}
	/**
	 * 返回对象的int型值.
	 * @param src
	 * @return
	 * @throws Exception
	 */
	public static int getInt(Object src) throws Exception
	{
		if(src==null){
			throw new Exception("转换对象不能为空");
		}
		try{
			if(src instanceof Integer)
				return ((Integer)src).intValue();
			else
				return Double.valueOf(getNumberString(getString(src))).intValue();
		}catch(Exception e){
			throw new Exception("转换对象为int类型发生错误,原因："+e.getMessage(),e);
		}
	}
	/**
	 * 返回对象的int型值.
	 * @param src
	 * @param def 对象为空时的返回值.
	 * @return
	 * @throws Exception
	 */
	public static int getInt(Object src,int def)
	{
		if(src!=null)	
		{
			try
			{
				return getInt(src);	
			}
			catch(Exception e)
			{
				return def;	
			}
				
		}				
		else
			return def;		
	}
	/**
	 * 返回对象的int型值.
	 * @param src
	 * @return
	 * @throws Exception
	 */
	public static long getLong(Object src) throws Exception
	{
		if(src==null){
			throw new Exception("转换对象不能为空");
		}
		try{
			if(src instanceof Long)
				return ((Long)src).longValue();
			else
				return Long.valueOf(getNumberString(getString(src))).longValue();
		}catch(Exception e){
			throw new Exception("转换对象为int类型发生错误,原因："+e.getMessage(),e);
		}
	}
	/**
	 * 返回对象的int型值.
	 * @param src
	 * @param def 对象为空时的返回值.
	 * @return
	 * @throws Exception
	 */
	public static long getLong(Object src,long def)
	{
		if(src!=null)	
		{
			try
			{
				return getLong(src);	
			}
			catch(Exception e)
			{
				return def;	
			}
				
		}				
		else
			return def;		
	}
	/**
	 * 返回对象的byte型值.
	 * @param src
	 * @return
	 * @throws Exception
	 */
	public static byte getByte(Object src) throws Exception
	{
		if(src==null){
			throw new Exception("转换对象不能为空");
		}
		try{
			if(src instanceof Byte)
				return ((Byte)src).byteValue();
			else
				return Double.valueOf(getNumberString(getString(src))).byteValue();
		}catch(Exception e){
			throw new Exception("转换对象为byte类型发生错误,原因："+e.getMessage(),e);
		}
	}
	/**
	 * 返回对象的byte型值.
	 * @param src
	 * @param def 对象为空时的返回值.
	 * @return
	 * @throws Exception
	 */
	public static byte getByte(Object src,byte def)
	{
		if(src!=null)	
		{
			try
			{
				return getByte(src);	
			}
			catch(Exception e)
			{
				return def;	
			}
				
		}				
		else
			return def;		
	}
	/**
	 * 返回对象的float型值.
	 * @param src
	 * @return
	 * @throws Exception
	 */
	public static float getFloat(Object src) throws Exception
	{
		if(src==null)
		{
			throw new Exception("转换对象不能为空");
		}
		try
		{
			if(src instanceof Float)
				return ((Float)src).floatValue();
			else
				return Double.valueOf(getNumberString(getString(src))).floatValue();
		}
		catch(Exception e)
		{
			throw new Exception("转换对象为float类型发生错误,原因："+e.getMessage(),e);
		}
	}
	/**
	 * 返回对象的float型值.
	 * @param src
	 * @param def 对象为空时的返回值.
	 * @return
	 * @throws Exception
	 */
	public static float getFloat(Object src,float def)
	{
		if(src!=null)	
		{
			try
			{
				return getFloat(src);	
			}
			catch(Exception e)
			{
				return def;	
			}				
		}				
		else
			return def;	
	}
	/**
	 * 返回对象的double型值.
	 * @param src
	 * @return
	 * @throws Exception
	 */
	public static double getDouble(Object src) throws Exception
	{
		if(src==null)
		{
			throw new Exception("转换对象不能为空");
		}
		try
		{
			if(src instanceof Double)
				return ((Double)src).doubleValue();
			else
				return Double.valueOf(getNumberString(getString(src))).doubleValue();
		}
		catch(Exception e)
		{
			throw new Exception("转换对象为double类型发生错误,原因："+e.getMessage());
		}
	}
	/**
	 * 返回对象的double型值.
	 * @param src
	 * @param def 对象为空时的返回值.
	 * @return
	 * @throws Exception
	 */
	public static double getDouble(Object src,double def)
	{
		if(src!=null)	
		{
			try
			{
				return getDouble(src);	
			}
			catch(Exception e)
			{
				return def;	
			}				
		}				
		else
			return def;
	}	
	/**
	 * 返回对象固定小数位的double型值.
	 * @param src
	 * @param def 对象为空时的返回值.
	 * @return
	 * @throws Exception
	 */
	public static double getDouble(Object src,double def,int dot)
	{
		if(src!=null)	
		{
			try
			{
				return round(getDouble(src),dot);	
			}
			catch(Exception e)
			{
				return def;	
			}				
		}				
		else
			return def;
	}	
	public static double round(double number,int bit){
		BigDecimal bd = new BigDecimal(number);
		BigDecimal one = new BigDecimal(1);
		double result = bd.divide(one, bit, BigDecimal.ROUND_HALF_UP).doubleValue();
		return result;
	}
	/**
	 * 获得当前日期.
	 * @return
	 */
	public static Date getNowDate() throws Exception
	{
		Calendar now = Calendar.getInstance();		
		if(now.getTimeZone().getID().equals("GMT")){
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
			SimpleTimeZone zone = new SimpleTimeZone(28800000,"GMT");
			df.setTimeZone(zone);
			String gmt8 = df.format(now.getTime());			
			now.setTime(parseDate(gmt8,"yyyy-MM-dd HH:mm:ss:SSS"));
		}		
		return now.getTime();			
	}	
	
	/**
	 * 获得当前日期.
	 * @return
	 */
	public static Date getToday() throws Exception
	{
		Date today =parseDate(getDateString());	
		return today;		
	}	
	/**
	 * 按格式获得当前日期.
	 * @return
	 * @throws Exception
	 */
	public static Date getNowDate(String dateFormat) throws Exception
	{
		SimpleDateFormat myFormat = new SimpleDateFormat(dateFormat);		
		String dateString = myFormat.format(getNowDate());
		return myFormat.parse(dateString);
	}
	/**
	 * 将字符串解析为日期
	 * @param dateString
	 * @return
	 * @throws Exception
	 */
	public static Date parseDate(String dateString) throws Exception
	{
		SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
		return myFormat.parse(dateString);
	}
	/**
	 * 按格式将字符串解析为日期
	 * @param dateString
	 * @param formatString
	 * @return
	 * @throws Exception
	 */
	public static Date parseDate(String dateString,String dateFormat) throws Exception
	{
		int len = dateString.length();
		dateFormat = dateFormat.substring(0, len);
		SimpleDateFormat myFormat = new SimpleDateFormat(dateFormat);		
		return myFormat.parse(dateString);
	}
	public static Date parseDate(long time) throws Exception{
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		return cal.getTime();
	}
	
	public static String getNextDay(String date){
		String day = "";
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTime(Convert.parseDate(date, "yyyy-MM-dd"));
			cal.add(Calendar.DAY_OF_MONTH, +1);
			day = Convert.getDateString(cal.getTime());
		}catch(Exception e){
			e.printStackTrace();			
		}		
		return day;
	}
	/**
	 * 按格式返回日期字符串.
	 * @param dateFormat
	 * @return
	 * @throws Exception
	 */
	public static String getDateString(String dateFormat) throws Exception
	{
		SimpleDateFormat myFormat = new SimpleDateFormat(dateFormat);		
		String nowstr = myFormat.format(getNowDate());
		return nowstr;
	}
	/**
	 * 按格式返回日期字符串.
	 * @param srcDate
	 * @param dateFormat
	 * @return
	 * @throws Exception
	 */
	public static String getDateString(Date srcDate,String dateFormat) throws Exception
	{
		if(srcDate == null)
			return "";
		SimpleDateFormat myFormat = new SimpleDateFormat(dateFormat);		
		String nowstr = myFormat.format(srcDate);
		return nowstr;
	}
	/**
	 * 按"yyyy-mm-dd"格式返回日期字符串.
	 * @return
	 * @throws Exception
	 */
	public static String getDateString() throws Exception
	{
		return getDateString("yyyy-MM-dd");
	}
	/**
	 * 按"yyyy-mm-dd"格式返回日期字符串.
	 * @param srcDate
	 * @return
	 * @throws Exception
	 */
	public static String getDateString(Date srcDate) throws Exception
	{
		return getDateString(srcDate,"yyyy-MM-dd");
	}	
	
	public static String getDateTimeString(String srcDate) throws Exception{
		Date date = parseDate(srcDate, "yyyy-MM-dd HH:mm:ss");
		return getDateTimeString(date);
	}
	/**
	 * 按"yyyy-mm-dd HH:mm:ss"格式返回日期字符串.
	 * @return
	 * @throws Exception
	 */
	public static String getDateTimeString() throws Exception
	{
		return getDateString("yyyy-MM-dd HH:mm:ss");
	}	
	/**
	 * 按"yyyy-mm-dd HH:mm:ss"格式返回日期字符串.
	 * @param srcDate
	 * @return
	 * @throws Exception
	 */
	public static String getDateTimeString(Date srcDate) throws Exception
	{
		return getDateString(srcDate,"yyyy-MM-dd HH:mm:ss");
	}
	//判断是否是日期.
	public static boolean isDate(String dateString){
		if(dateString== null || dateString.trim().equals("")){
			return false;
		}
		try{
			Convert.parseDate(dateString);
			return true;
		}catch(Exception e){
			return false;
		}	
	}
}
