package com.feng.util;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class ImageUtil {


    public final static BufferedImage resize(BufferedImage source,int targetW,int targetH,boolean equalProportion){  
    	int type=source.getType();  
        BufferedImage target=null;  
        double sx=(double)targetW/source.getWidth();  
        double sy=(double)targetH/source.getHeight();  
        //这里想实现在targetW，targetH范围内实现等比例的缩放  
        //如果不需要等比例的缩放则下面的if else语句注释调即可  
        if(equalProportion){  
            if(sx>sy){  
                sx=sy;  
                targetW=(int)(sx*source.getWidth());  
            }else{  
                sy=sx;  
                targetH=(int)(sx*source.getHeight());  
            }  
        }  
        if(type==BufferedImage.TYPE_CUSTOM){          	
            ColorModel cm=source.getColorModel();  
            WritableRaster raster=cm.createCompatibleWritableRaster(targetW,targetH);  
            boolean alphaPremultiplied=cm.isAlphaPremultiplied();  
            target=new BufferedImage(cm,raster,alphaPremultiplied,null);  
        }else{          	
            target=new BufferedImage(targetW,targetH,type);             
        }         
        Graphics2D g=target.createGraphics();  
        g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);  
        g.drawRenderedImage(source,AffineTransform.getScaleInstance(sx,sy));  
        g.dispose();  
        return target;  
    }  
   
    /**
     * 缩放图像（按高度和宽度缩放）
     * @param input 源图像流
     * @param outFile 缩放后的图像地址
     * @param width 宽度
     * @param height 高度
     */
    public final static void resize(File srcImage, String outFile, int width,int height) {
        try {                   
            BufferedImage bi = ImageIO.read(srcImage);            
            BufferedImage  bo = resize(bi,width,height,false);      
            if(outFile.toLowerCase().endsWith(".png")){
            	ImageIO.write(bo, "PNG", new File(outFile));
            }else{
            	ImageIO.write(bo, "JPEG", new File(outFile));
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }   
    
    /**
     * 缩放图像（按高度和宽度缩放）
     * @param input 源图像流
     * @param outFile 缩放后的图像地址
     * @param width 宽度
     * @param height 高度
     */
    public final static void resize(InputStream srcInput, String outFile, int width,int height) {
        try {                   
            BufferedImage bi = ImageIO.read(srcInput);            
            BufferedImage  bo = resize(bi,width,height,false);            
            if(outFile.toLowerCase().endsWith(".png")){
            	ImageIO.write(bo, "PNG", new File(outFile));
            }else{
            	ImageIO.write(bo, "JPEG", new File(outFile));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }   
    
}