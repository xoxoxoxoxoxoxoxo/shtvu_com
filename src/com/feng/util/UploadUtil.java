package com.feng.util;

import java.io.File;
import java.util.Random;
import java.util.regex.Pattern;


import com.jfinal.core.JFinal;

public abstract class UploadUtil {	
	
	//判断是否OSS环境.
	public static boolean isOSS() {
//		return true;
		return false;
	}
	//格式话路径信息
	public static String formatPath(String path){		
		if(path == null || path.trim().equals("")){
			return "";
		}
		return (path.endsWith("\\") || path.endsWith("/")) ? path : path+"/";		
	}
	
	//返回上传文件保存的路径.
	public static String getSavePath(String destPath){
		String tempPath = JFinal.me().getServletContext().getRealPath("/") + "/upload/"+formatPath(destPath);
		System.out.println(tempPath);
		return tempPath;
	}
	
	//判断是否是合法的图片文件.
	public static boolean checkImg(String fileName) {
		return Pattern.compile(".+?\\.(jpg|gif|bmp|png|jpeg)").matcher(fileName.toLowerCase()).matches();
	}
	
	//获取文件扩展名
	public static String getFileExt(String fileName) {
		return fileName.substring(fileName.lastIndexOf("."));
	}
	
	//随机产生文件名
	public static String randFileName(String fileName) {
		Random random = new Random();
		return fileName = "" + random.nextInt(10000)	+ System.currentTimeMillis() + getFileExt(fileName);
	}
	
	//从全路径中获取文件名称.
	public static String parseFileName(String filePath){
		if (filePath.contains("\\")) {
			filePath = filePath.substring(filePath.lastIndexOf("\\") + 1);
		}else {
			filePath = filePath.substring(filePath.lastIndexOf("/") + 1);
		}
		return filePath;
	}	
	
	public static void deleteExist(String path) {
		File file = new File(path);
		if (file.exists()) {
			file.delete();
		}
	}
}
