package com.feng.util;

import java.util.LinkedHashMap;
import java.util.Map;

import com.jfinal.kit.JsonKit;

public class AjaxResult {
	private Map<String,Object> result = new LinkedHashMap<String,Object>();	
	public AjaxResult(int status) {		
		result.put("status", status);		
	}
	public AjaxResult(int status, String info) {
		result.put("status", status);	
		result.put("info", info);			
	}
	
	public void setStatus(int status){
		result.put("status", status);		
	}
	
	public void setInfo(String info){
		result.put("info", info);	
	}
	
	public void setData(String name,Map data) {
		result.put(name, data);
	}
	public void setData(String name,String value){
		result.put(name, value);
	}
	public void setData(String name,Object value){
		result.put(name, value);
	}
	
	public void addError(String error) {
		this.setStatus(0);
		this.setInfo(error);
	}
	public String toJson(){
		return JsonKit.toJson(result);
	}
	
	
}
