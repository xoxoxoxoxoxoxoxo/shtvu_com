package com.feng.controller;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.feng.util.AjaxResult;
import com.feng.util.ImageUtil;
import com.feng.util.UploadUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jfinal.core.Controller;

public class BaseController extends Controller{
	public final String MESSAGE="<p><strong>没有内容!</strong><br/>请到后台完成编辑即可正常显示！</p>";
	private static final int MAX_IMG_SIZE = 5242880;
	private static final Map<String, Integer[]> IMG_SIZE = new HashMap<String, Integer[]>();
	
	public BaseController() {
		//不同业务场景原始图片生成缩略图规格
		IMG_SIZE.put("pro", new Integer[]{700, 400});
		IMG_SIZE.put("news", new Integer[]{640, 400});
		IMG_SIZE.put("slide", new Integer[]{940, 400});
	}
	
	public void renderAjaxOk(){
		AjaxResult result = new AjaxResult(1);
		this.renderJson(result.toJson());
	}
	
	public void renderAjaxError(String msg){
		AjaxResult result = new AjaxResult(0);
		result.setInfo(msg);
		this.renderJson(result.toJson());
	}

	public Map<String, String> fromJson(String value) throws Exception {
		Gson gson = new Gson();
		
		return gson.fromJson(value, new TypeToken<Map<String, String>>(){}.getType());
	}
	
	public void uploadImg() throws Exception{		
		AjaxResult result = new AjaxResult(1);		
		try {
			String path = this.getPara("path", "");
			
			String fileName = this.saveImageFile("file", path, "");
			
			result.setData("fileUrl", fileName);
		} catch (Exception e) {
			e.printStackTrace();
			result.addError("上传图片失败:"+e.getMessage());
		}	
		
		this.renderJson(result.toJson());
	}
	
	/**
	 * 保存客户端上传的图片文件 
	 * @param field  客户端表单的file字段名
	 * @param destFilePath  目标文件路径
	 * @param fileName 指定图片名称
	 * @param key 图片应用场景
	 * @return 相对于当前用户根目录的图片路径.
	 */
	protected String saveImageFile(String field,String destFilePath, String fileName){
		
		String tempPath = UploadUtil.getSavePath(destFilePath);
		File file = this.getFile(field, tempPath, MAX_IMG_SIZE).getFile();
		
		//检测文件是否合法
		if(!UploadUtil.checkImg(file.getName())){
			file.delete();
			throw new RuntimeException("图片文件格式不合法，只接受【jpg|gif|bmp|png|jpeg】格式的图片");
		}
		
		fileName = fileName.equals("") ? UploadUtil.randFileName(file.getName()) 
									   : fileName;
		
		//文件重命名
		file = this.fileReName(file, fileName, tempPath);
		//有key时生成缩略图
		String key = this.getPara("key", "");//不处理为空
		if (!key.equals("")) { //如果需要则处理图片
			this.resizeImg(file, key, tempPath, fileName);
		}
		
		return UploadUtil.formatPath(destFilePath)+fileName;		
	}
	
	public File fileReName(File file, String fileName, String path) {
		//重新命名
		if (!fileName.equals("")) {
//			fileName = fileName + UploadUtil.getFileExt(file.getName());
			
			File temp = new File(path + fileName);
			if (temp.exists()) temp.delete();
			
			file.renameTo(temp);
			
			file = temp;
		}
		return file;
	}
	//缩略图规格
	private void resizeImg(File file, String key, String path, String fileName) {
		int width = IMG_SIZE.get(key)[0]; //图片宽度
        int height = IMG_SIZE.get(key)[1]; //图片高度
        
        ImageUtil.resize(file, path + fileName,width,height);
	}
	
}
