package com.feng.controller.web;

import java.util.List;

import com.feng.controller.BaseController;
import com.feng.util.AjaxResult;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

/**
 * 前台主页
 * @author xiaozhi
 *
 */
public class IndexController extends BaseController {
	
	public void index(){
		/*
		 * 联系我们
		 * 废弃
		 */
//		Record intro=Db.findFirst("select * from com_introduce");
//		if(intro==null){
//			Map<String, String> info = new HashMap<String, String>();
//			info.put("title", "联系我们");
//			info.put("addr", "上海市长宁区天山路");
//			info.put("tel", "136xxxxxxx");
//			info.put("num", "021-22554477");
//			info.put("email", "304222428@qq.com");
//			setAttr("intro", info);
//		}else{
//			setAttr("intro", intro);
//		}
		/*
		 * 公司介绍
		 */
		Record cont=Db.findFirst("select * from com_connection");
		if(cont==null){
			setAttr("cont", MESSAGE);
		}else{
			setAttr("cont", cont.get("content"));
		}
		/*
		 * 获取前12条产品信息
		 */
		List<Record> pro = Db.find("select a.*,b.name as type " +
				"from com_product as a " +
					"left join com_pro_type as b on a.type_id=b.id " +
						"order BY a.create_time limit 0,12;");
		setAttr("pro", pro);
		/*
		 * 获取首页幻灯片
		 */
		List<Record> slide = Db.find("SELECT * FROM `com_slide` WHERE `status`=1");
		setAttr("slide", slide);
		render("newindex.ftl");
	}
	/**
	 * 获取新闻Top6
	 */
	public void newsTop(){
		AjaxResult result=new AjaxResult(1);
		String sql="FROM `com_news` ORDER BY `create_time` DESC";
		
		int pageNow = getParaToInt("page_no",1);
		Page<Record> news = Db.paginate(pageNow, 3, "select * ", sql.toString());
		
		result.setData("page", JsonKit.toJson(news,1));
		result.setData("rows", JsonKit.toJson(news.getList(),2));
		
		this.renderJson(result.toJson());
	}
}
