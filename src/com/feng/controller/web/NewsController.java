package com.feng.controller.web;


import com.feng.controller.BaseController;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
/**
 * 网站新闻
 * @author xiaozhi
 *
 */
public class NewsController extends BaseController {
	/**
	 *新闻分页列表 
	 */
	public void list(){
		int pageNumber = getParaToInt(0, 1);
		String sql= "from com_news";
		Page<Record> news=Db.paginate(pageNumber, 2,"select * ",sql.toString());
		setAttr("news", news.getList());
		setAttr("page", news);
		setAttr("ctx", getRequest().getContextPath());
		setAttr("pageUrl", "news/list");
		render("news_list.ftl");
	}

	/**
	 * 新闻内容
	 */
	public void info(){
		String id=getPara("id");
		Record newsInfo=Db.findFirst("select * from com_news where id="+id);
		setAttr("newsInfo", newsInfo);
		render("news_info.ftl");
	}
}
