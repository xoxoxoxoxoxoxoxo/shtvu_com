package com.feng.controller.web;

import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 产品中心
 * @author xiaozhi
 *
 */
public class ProductController extends Controller {

	/**
	 * 产品列表
	 */
	public void list(){
		List<Record> typeName=Db.find("select name from com_pro_type");
		setAttr("typeName", typeName);
		
		List<Record> pro = Db.find("select a.*,b.name as type " +
				"from com_product as a " +
					"left join com_pro_type as b on a.type_id=b.id " +
						"order BY a.create_time");
		setAttr("pro", pro);
		
		render("product_list.ftl");
	}
	/**
	 * 产品详情
	 */
	public void info(){
		int id = getParaToInt("id");
		Record proInfo=Db.findFirst("select a.*,b.name as type " +
										"from com_product as a " +
												"left join com_pro_type as b on a.type_id=b.id " +
													" where a.id="+id);
		setAttr("proInfo", proInfo);
		render("product_info.ftl");
	}
}
