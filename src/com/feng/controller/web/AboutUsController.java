package com.feng.controller.web;

import com.feng.controller.BaseController;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class AboutUsController extends BaseController {
	public void index(){
		/*
		 * 公司介绍
		 */
		Record cont=Db.findFirst("select * from com_connection");
		if(cont==null){
			setAttr("cont", MESSAGE);
		}else{
			setAttr("cont", cont);
		}
		render("about_us.ftl");
	}
}
