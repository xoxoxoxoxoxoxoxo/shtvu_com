package com.feng.controller.admin;

import java.util.Date;

import com.feng.dao.ComConnection;
import com.feng.util.AjaxResult;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
/**
 * 公司介绍
 * @author xiaozhi
 *
 */
public class ConnecController extends Controller {
	
	public void index(){
		Record cont=Db.findFirst("select * from com_connection");
		if(cont==null){
			setAttr("cont", cont);
		}else{
			setAttr("cont", cont.toJson());
		}
		this.render("connection.ftl");
	}
	
	public void edit(){
		AjaxResult result=new AjaxResult(1);
		try {
			ComConnection cont=this.getModel(ComConnection.class, "cont");
			if(cont.get("id","").equals("")){
				cont.set("title", "公司介绍").set("create_time", new Date()).save();
			}else{
				cont.update();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.addError(e.getMessage());
		}
		this.renderJson(result.toJson());
	}

}
