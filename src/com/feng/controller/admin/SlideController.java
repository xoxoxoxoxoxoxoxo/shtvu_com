package com.feng.controller.admin;

import com.feng.controller.BaseController;
import com.feng.dao.ComSlide;
import com.feng.util.AjaxResult;
import com.feng.util.Convert;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

/**
 * 图片系统
 * @author xiaozhi
 *
 */
public class SlideController extends BaseController {

	/*
	 * 首页幻灯片列表
	 */
	public void slideList(){
		render("slide_list.ftl");
	}
	/*
	 * 幻灯片
	 */
	public void slideData(){
		AjaxResult result = new AjaxResult(1);
		String sql="from com_slide where status=1 order by sort asc";
		
		int pageNow = getParaToInt("page_no",1);
		Page<Record> slide = Db.paginate(pageNow,10,"select * ",sql.toString());
			
		result.setData("page", JsonKit.toJson(slide,1));
		result.setData("rows", JsonKit.toJson(slide.getList(),2));
		
		this.renderJson(result.toJson());
	}
	/*
	 * 幻灯片编辑
	 */
	public void slideEdit(){
		AjaxResult result = new AjaxResult(1);
		
		try {
			ComSlide slide =getModel(ComSlide.class, "slide");
			if(slide.get("id", "").equals("")){
				slide.set("create_time", Convert.getDateTimeString()).save();
			}else{
				slide.update();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.addError(e.getMessage());
		}
		this.renderJson(result.toJson());
	}
	/*
	 * 幻灯片信息
	 */
	public void slideInfo(){
		String id=getPara("id", "");
		if(!id.equals("")){
			Record slideInfo=Db.findFirst("select * from com_slide where id="+id);
			setAttr("slideInfo", slideInfo.toJson());
		}
		render("slide_info.ftl");
	}
	/*
	 *幻灯片删除 
	 */
	public void slideDelete(){
		AjaxResult result = new AjaxResult(1);
		String id=getPara("id");
		ComSlide.dao.deleteById(id);
		this.renderJson(result.toJson());
	}
}
