package com.feng.controller.admin;


import java.util.Calendar;
import java.util.List;

import com.feng.controller.BaseController;
import com.feng.dao.ComNews;
import com.feng.dao.ComNewsType;
import com.feng.util.AjaxResult;
import com.feng.util.Convert;
import com.feng.validator.NewsValidator;
import com.jfinal.aop.Before;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.shiro.SH;

/**
 * 新闻系统
 * @author xiaozhi
 *
 */
public class ComNewsController extends BaseController {

	/**
	 * 文章列表
	 */
	public void newsList(){
		render("news_list.ftl");
	}
	/**
	 * 文章
	 */
	public void newsData(){
		AjaxResult result = new AjaxResult(1);
		String sql="from com_news as a " +
					"LEFT JOIN com_news_type AS b ON a.type_id=b.id " +
					"LEFT JOIN users as c ON a.create_man=c.id";
		
		int pageNow = getParaToInt("page_no",1);
		Page<Record> brand = Db.paginate(pageNow,10,"SELECT a.*, b. NAME AS typeName,c.user_name as create_man",sql.toString());
			
		result.setData("page", JsonKit.toJson(brand,1));
		result.setData("rows", JsonKit.toJson(brand.getList(),2));
		
		this.renderJson(result.toJson());
	}
	/**
	 * 文章详情
	 */
	public void newsInfo(){
		String id=getPara("id", "");
		if(!id.equals("")){
			Record newsInfo=Db.findFirst("select * from com_news where id="+id);
			setAttr("newsInfo", newsInfo.toJson());
		}
		render("news_info.ftl");
	}
	/**
	 * 文章编辑
	 */
	@Before(NewsValidator.class)//表单验证
	public void newsEdit(){
		AjaxResult result=new AjaxResult(1);
		Calendar calendar = Calendar.getInstance();
		int month = calendar.get(Calendar.MONTH)+1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		try {
			ComNews news=this.getModel(ComNews.class, "news");
			news.put("create_man", SH.getUser().getUserId()+"");
			if(news.get("id", "").equals("")){
				news.set("create_time", Convert.getDateString())
					.set("month", month).set("day", day).save();
				result.setData("id", news.get("id"));
				
			}else{
				news.update();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.addError(e.getMessage());
		}
		this.renderJson(result.toJson());
		
	}
	/**
	 * 文章删除
	 */
	public void newsDelete(){
		AjaxResult result=new AjaxResult(1);
		String id=getPara(0);
		ComNews.dao.deleteById(id);
		this.renderJson(result.toJson());
	}
	/**
	 * 类别列表
	 */
	public void newsType(){
		render("type_list.ftl");
	}
	/**
	 * 类别编辑
	 */
	public void typeEdit(){
		AjaxResult result=new AjaxResult(1);
		
		try {
			ComNewsType type=this.getModel(ComNewsType.class, "type");
			
			if(type.get("id", "").equals("")){
				//排序不能重复
				if(Db.findFirst("select count(1) as num from com_news_type where rec like "+
						type.get("rec")).getLong("num")>0){
					result.addError("排序数值不能重复，请重新编辑！");
				}else{
					type.save();
					result.setData("id", type.get("id"));
				}
			}else{
				type.update();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.addError(e.getMessage());
		}
		this.renderJson(result.toJson());
	}
	/**
	 * 分类
	 */
	public void typeData(){
		AjaxResult result=new AjaxResult(1);
		String sql="from com_news_type";
		int pageNow=getParaToInt("page_no",1);
		Page<Record> type = Db.paginate(pageNow, 10, "select * ",sql);
		result.setData("page", JsonKit.toJson(type, 1));
		result.setData("rows", JsonKit.toJson(type.getList(),2));
		this.renderJson(result.toJson());
	}
	/**
	 * 删除类别
	 */
	public void deleteType(){
		AjaxResult result=new AjaxResult(1);
		String id=getPara(0);
		String sql="select count(1) as num from com_news where type_id=" + id;
		if(Db.findFirst(sql).getLong("num")>0){
			result.addError("该品分类下仍有新闻，请先删除新闻！");
		}else{
			ComNewsType.dao.deleteById(id);
		}
		this.renderJson(result.toJson());
	}
	/**
	 *分类信息 
	 */
	public void typeInfo(){
		AjaxResult result = new AjaxResult(1);
		Integer id=getParaToInt("id");
		String sql="select * from com_news_type where id=?";
		Record typeInfo=Db.findFirst(sql,id);
		result.setData("type", typeInfo.toJson());
		this.renderJson(result.toJson());
	}
	/**
	 * 文章获取分类
	 */
	public void getType(){
		List<Record> type=Db.find("select id,name from com_news_type");
		renderJson(type);
	}
}
