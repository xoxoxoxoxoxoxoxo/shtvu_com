package com.feng.controller.admin;

import com.jfinal.core.Controller;
import com.jfinal.shiro.SH;
import com.jfinal.shiro.ShiroRealm.ShiroUser;

/**
 * 
 * @author xiaozhi
 *	
 *	后台管理
 */
public class IndexController extends Controller {

	public void index(){
		render("login.ftl");
	}
	/**
	 * 验证登陆
	 */
	public void login(){
		if("post".equalsIgnoreCase(this.getRequest().getMethod())){	
			String userName = this.getPara("user_name","");
			String password = this.getPara("password","");		
			String result = SH.login(userName, password, false);
			ShiroUser user = null;
			//验证成功后跳转
			if(result.equalsIgnoreCase("success")){
				user = (ShiroUser)SH.getSubject().getPrincipal();
				this.setAttr("user", user);
				this.setAttr("isLogin", true);
				this.render("index.ftl");
			}else{
				//返回错误信息
				this.setAttr("error", "用户名或密码不正确");
				this.render("login.ftl");
			}
		}else{						
			this.render("login.ftl");
		}		
	}
	//登出操作
	public void logout() {
		try {
			SH.logout();
		} catch (Exception e) {
			
		}
		
		this.redirect("/admin/login");
	}
}
