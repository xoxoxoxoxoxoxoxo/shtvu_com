package com.feng.controller.admin;

import java.util.List;

import com.feng.controller.BaseController;
import com.feng.dao.ComProType;
import com.feng.dao.ComProduct;
import com.feng.util.AjaxResult;
import com.feng.util.Convert;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

public class ProductController extends BaseController {

	/*
	 * 产品列表
	 */
	public void proList(){
		render("product_list.ftl");
	}
	
	public void proData(){
		AjaxResult result = new AjaxResult(1);
		String sql="from com_product as a left join com_pro_type as b on b.id=a.type_id ";
		
		int pageNow = getParaToInt("page_no",1);
		Page<Record> pdt = Db.paginate(pageNow,10,"select a.*,b.name as typeName",sql.toString());
			
		result.setData("page", JsonKit.toJson(pdt,1));
		result.setData("rows", JsonKit.toJson(pdt.getList(),2));
		
		this.renderJson(result.toJson());
	}
	/*
	 *产品编辑 
	 */
	public void proEdit(){
		AjaxResult result=new AjaxResult(1);
		
		try {
			ComProduct pdt=getModel(ComProduct.class, "pdt");
			if(pdt.get("id", "").equals("")){
				pdt.set("create_time", Convert.getDateString()).save();
			}else{
				pdt.update();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.addError(e.getMessage());
		}
		this.renderJson(result.toJson());
	}
	/*
	 * 产品详情
	 */
	public void proInfo(){
		String id=getPara("id", "");
		if(!id.equals("")){
			Record newsInfo=Db.findFirst("select * from com_product where id="+id);
			setAttr("proInfo", newsInfo.toJson());
		}
		render("product_info.ftl");
	}
	/*
	 * 产品删除
	 */
	public void proDelete(){
		AjaxResult result=new AjaxResult(1);
		String id=getPara(0);
		ComProduct.dao.deleteById(id);
		this.renderJson(result.toJson());
	}
	/*
	 * 类别列表
	 */
	public void typeList(){
		render("type_list.ftl");
	}
	/*
	 * 类别编辑
	 */
	public void typeEdit(){
		AjaxResult result=new AjaxResult(1);
		
		try {
			ComProType type=this.getModel(ComProType.class, "type");
			
			if(type.get("id", "").equals("")){
					type.save();
					result.setData("id", type.get("id"));
			}else{
				type.update();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.addError(e.getMessage());
		}
		this.renderJson(result.toJson());
	}
	/*
	 * 类别
	 */
	public void typeData(){
		AjaxResult result=new AjaxResult(1);
		String sql="from com_pro_type";
		int pageNow=getParaToInt("page_no",1);
		Page<Record> type = Db.paginate(pageNow, 10, "select * ",sql);
		result.setData("page", JsonKit.toJson(type, 1));
		result.setData("rows", JsonKit.toJson(type.getList(),2));
		this.renderJson(result.toJson());
	}
	/*
	 * 删除类别
	 */
	public void deleteType(){
		AjaxResult result=new AjaxResult(1);
		String id=getPara(0);
		String sql="select count(1) as num from com_product where type_id=" + id;
		if(Db.findFirst(sql).getLong("num")>0){
			result.addError("该分类下仍有产品，请先删除产品！");
		}else{
			ComProduct.dao.deleteById(id);
		}
		this.renderJson(result.toJson());
	}
	/*
	 *类别信息 
	 */
	public void typeInfo(){
		AjaxResult result = new AjaxResult(1);
		Integer id=getParaToInt("id");
		String sql="select * from com_pro_type where id=?";
		Record typeInfo=Db.findFirst(sql,id);
		result.setData("type", typeInfo.toJson());
		this.renderJson(result.toJson());
	}
	/*
	 * 文章获取类别
	 */
	public void getType(){
		List<Record> type=Db.find("select id,name from com_pro_type");
		renderJson(type);
	}
}
