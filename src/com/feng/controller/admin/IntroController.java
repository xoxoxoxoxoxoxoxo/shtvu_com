package com.feng.controller.admin;


import com.feng.dao.ComIntroduce;
import com.feng.util.AjaxResult;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 联系我们
 * @author xiaozhi
 *	
 */
public class IntroController extends Controller {

	public void index(){
		Record intro=Db.findFirst("select * from com_introduce");
		if(intro==null){
			this.setAttr("intro", intro);
		}else{
			this.setAttr("intro", intro.toJson());
			
		}
		render("intro.ftl");
	}
	/**
	 * 只在原有信息上修改
	 */
	public void edit(){
		AjaxResult result=new AjaxResult(1);
		
		try {
			ComIntroduce intro=this.getModel(ComIntroduce.class, "intro");
			if(intro.get("id", "").equals("")){
				intro.set("title", "联系我们").save();
			}else{
				intro.update();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.addError(e.getMessage());
		}
		this.renderJson(result.toJson());
	}
}
