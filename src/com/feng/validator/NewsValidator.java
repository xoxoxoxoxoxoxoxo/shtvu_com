package com.feng.validator;


import com.feng.controller.BaseController;
import com.feng.util.Validator;
import com.jfinal.core.Controller;

public class NewsValidator extends AjaxValidator {

	@Override
	protected void handleError(Controller c) {
		BaseController controller = (BaseController)c;
		controller.renderAjaxError(getLastMessage());
	}

	@Override
	protected void validate(Controller c) {
		this.setShortCircuit(true);
		try {
			if(!Validator.validateString(c.getPara("news.title"), 0, 30)){
				this.addError("info", "保存失败：文章标题不正确！");
			}
			if(c.getPara("news.type_id").length()==0){
				this.addError("info", "保存失败：文章类别未关联！");
			}
			if(!Validator.validateString(c.getPara("news.image"), 0, 200)){
				this.addError("info", "保存失败：封面图片不正确！");
			}
			if(!Validator.validateString(c.getPara("news.summary"), 0, 100)){
				this.addError("info", "保存失败：摘要信息错误！");
			}
			if(c.getPara("news.content","").length()>20000){
				this.addError("info", "保存失败：文章内容字数超长！");
			}
			if(!Validator.validateString(c.getPara("news.content"), 0, 20000)){
				this.addError("info", "保存失败：文章内容不能为空！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (e.getMessage() != null) {
				this.addError("info", "保存失败：" + e.getMessage());
			}
		}

	}

}
