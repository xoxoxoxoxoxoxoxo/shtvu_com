package com.feng.common;

import com.feng.controller.admin.ComNewsController;
import com.feng.controller.admin.ConnecController;
import com.feng.controller.admin.IntroController;
import com.feng.controller.admin.ProductController;
import com.feng.controller.admin.SlideController;
import com.feng.controller.web.AboutUsController;
import com.feng.controller.web.NewsController;
import com.feng.dao.ComConnection;
import com.feng.dao.ComIntroduce;
import com.feng.dao.ComNews;
import com.feng.dao.ComNewsType;
import com.feng.dao.ComProType;
import com.feng.dao.ComProduct;
import com.feng.dao.ComSlide;
import com.feng.dao.Users;
import com.feng.util.UpdateDB;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.tx.TxByRegex;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;
/**
 * 
 * @author xiaozhi
 *	2015-04-20
 */
public class ComConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		//开发者模式，控制台打印Jfinal report
		me.setDevMode(true);
		//默认视图
		me.setViewType(ViewType.FREE_MARKER);
		//视图默认路径
		me.setBaseViewPath("/WEB-INF/view");
		//视图后缀
		me.setFreeMarkerViewExtension(".ftl");
//		me.setUploadedFileSaveDirectory(PathKit.getWebRootPath() +"/upload");
	}

	@Override
	public void configHandler(Handlers me) {

	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new AuthInterceptor());
		me.add(new TxByRegex("(.*add*.*)|(.*delete*.*)|(.*edit*.*)|(.*update*.*)|(.*cancel*.*)|(.*create*.*)"));
	}

	@Override
	public void configPlugin(Plugins me) {
		//加载配置文件
		this.loadPropertyFile("jdbc.properties");

		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password").trim());
		me.add(c3p0Plugin);
		c3p0Plugin.start();
		//自动检测数据库
		UpdateDB.update(c3p0Plugin);
		//多数据源
		ActiveRecordPlugin arp = new ActiveRecordPlugin("zero",c3p0Plugin);
		arp.setShowSql(true);
		
		me.add(arp);
		arp.addMapping("users", Users.class);
		arp.addMapping("com_introduce", ComIntroduce.class);
		arp.addMapping("com_connection", ComConnection.class);
		arp.addMapping("com_news", ComNews.class);
		arp.addMapping("com_news_type", ComNewsType.class);
		arp.addMapping("com_product", ComProduct.class);
		arp.addMapping("com_pro_type", ComProType.class);
		arp.addMapping("com_slide", ComSlide.class);
		arp.start();
	}

	@Override
	public void configRoute(Routes me) {
		//网站前台
		me.add("/", com.feng.controller.web.IndexController.class, "web");
		me.add("/about", AboutUsController.class, "web");
		me.add("/news",NewsController.class,"web/news");
		me.add("/product", com.feng.controller.web.ProductController.class,"web/product");
		//后台管理
		me.add("/admin", com.feng.controller.admin.IndexController.class, "admin");
		//ViewPath=ControllerKey
		me.add("/admin/intro", IntroController.class);
		me.add("/admin/cont", ConnecController.class, "/admin/connection/");
		me.add("/admin/news", ComNewsController.class);
		me.add("/admin/product", ProductController.class);
		me.add("/admin/slide", SlideController.class,"/admin/picture");
	}

	public static void main(String[] args) {
		JFinal.start("WebRoot", 8081, "/", 5);
	}
}
