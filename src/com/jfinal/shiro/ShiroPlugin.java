package com.jfinal.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;

import com.jfinal.plugin.IPlugin;

public class ShiroPlugin  implements IPlugin {

	/**
	 * 构造函数
	 * @param routes 路由设定
	 */
	public ShiroPlugin(){		
	}

	/**
	 * 停止插件
	 */
	public boolean stop() {
		return true;
	}

	/**
	 * 启动插件
	 */
	public boolean start() {
		ShiroRealm realm = new ShiroRealm();		
		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager(realm);	
		
		DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();		
		sessionManager.setSessionValidationSchedulerEnabled(true);
		sessionManager.setGlobalSessionTimeout(180000);			
		securityManager.setSessionManager(sessionManager);			

		
		SecurityUtils.setSecurityManager(securityManager);			
		return true;
	}	
}
